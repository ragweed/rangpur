﻿DeclareModule Config
  
  Declare LoadConfig()
  Declare SaveConfig()

  ; Здесь будем хранить данные авторизации.
  Global Token.s, Secret.s, UserID.l, UserFirstName.s, UserLastName.s, UserPhotoURL.s

  ; Здесь будем хранить путь до БД.
  Global DatabasePath.s
  
  ; А здесь будем хранить путь до браузера.
  Global WebBrowserPath.s
  
  ; А здесь будем хранить id альбома, который слушал пользователь перед закрытием программы.
  Global LastAlbum.l
  
  ; А это значение громкости.
  Global Volume.l
  
  ; А если эта переменная равна 1, то при сохранении трека он будет сразу добавляться в 
  ; "Мои аудиозаписи" без появления #Window_SelectAlbums, даже при наличии альбомов.
  Global IgnoreSAW.a
  
  ; Если равна 1, значит необходимо предложить пользователь выбрать подписки.
  Global FirstAuth.a
  
  ; Если равна 1, то при сохранении аудиозаписи, пост из которого она была взята будет добавлен
  ; в список 'Мне нравится'.
  Global AutoLikePost.a
  
  ; Если равна 1, то Rangpur будет проверять подписки на наличие новых аудиозаписей при старте приложения.
  Global CheckSubAtStartup.a
  
EndDeclareModule

Module Config
  
  ; Загружаем настройки из конфига
  Procedure LoadConfig()
    
    If OpenPreferences("config.cfg")
      open = 1
    EndIf
    
    PreferenceGroup("VK")
    Token.s = ReadPreferenceString("Token", "")
    Secret.s = ReadPreferenceString("Secret", "")
    UserID.l = ReadPreferenceLong("UserID", 0)
    
    UserFirstName.s = ReadPreferenceString("UserFirstName", "")
    UserLastName.s = ReadPreferenceString("UserLastName", "")
    UserPhotoURL.s = ReadPreferenceString("UserPhotoURL", "")

    LastAlbum.l = ReadPreferenceLong("LastAlbum", -1)
    IgnoreSAW.a = ReadPreferenceLong("IgnoreSelectAlbumsWindow", 0)
    AutoLikePost.a = ReadPreferenceLong("AutoLikePost", 1)
    FirstAuth.a = ReadPreferenceLong("FirstAuth", 1)
    CheckSubAtStartup.a = ReadPreferenceLong("CheckSubAtStartup", 1)
    
    PreferenceGroup("Database")
    DatabasePath.s = ReadPreferenceString("DB_Path","database.sqlite3")
    
    PreferenceGroup("Programs")
    WebBrowserPath.s = ReadPreferenceString("WebBrowser","default")
    
    PreferenceGroup("GUI")
    GUI::PlayerMode = ReadPreferenceLong("PlayerMode", GUI::#Player_Feed)
    GUI::DefaultArtwork.a = ReadPreferenceLong("DefaultArtwork", 1)
    GUI::ArtworkPath.s = ReadPreferenceString("ArtworkPath", "")
    
    PreferenceGroup("Player")
    Volume.l = ReadPreferenceLong("Volume", 100)

    If open = 1
      ClosePreferences()
    EndIf

  EndProcedure
  
  Procedure SaveConfig()
    If OpenPreferences("config.cfg")
      open=1
    Else
      If CreatePreferences("config.cfg")
        open=1
      EndIf
    EndIf
    
    If open=1
      PreferenceGroup("VK")
      WritePreferenceString("Token", Token.s)
      WritePreferenceString("Secret", Secret.s)
      WritePreferenceLong("UserID", UserID.l)
      WritePreferenceString("UserFirstName", UserFirstName.s)
      WritePreferenceString("UserLastName", UserLastName.s)
      WritePreferenceString("UserPhotoURL", UserPhotoURL.s)
      WritePreferenceLong("LastAlbum", LastAlbum.l)
      WritePreferenceLong("IgnoreSelectAlbumsWindow", IgnoreSAW.a)
      WritePreferenceLong("AutoLikePost", AutoLikePost.a)
      WritePreferenceLong("FirstAuth", FirstAuth.a)
      WritePreferenceLong("CheckSubAtStartup", CheckSubAtStartup.a)

      PreferenceGroup("Database")
      WritePreferenceString("DB_Path", DatabasePath.s)
      
      PreferenceGroup("Programs")
      WritePreferenceString("WebBrowser", WebBrowserPath.s)

      PreferenceGroup("GUI")
      WritePreferenceLong("PlayerMode", GUI::PlayerMode.l)
      WritePreferenceLong("DefaultArtwork", GUI::DefaultArtwork.a)
      WritePreferenceString("ArtworkPath", GUI::ArtworkPath.s)

      PreferenceGroup("Player")
      WritePreferenceLong("Volume", GetGadgetState(GUI::#GW_TrackBar_Volume))
      
      ClosePreferences()
    EndIf
  EndProcedure
 
EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 102
; FirstLine = 91
; Folding = -
; EnableUnicode
; EnableXP