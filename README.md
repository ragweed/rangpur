# Rangpur #

### Что это такое? ###

Попытка создать плеер для [социальной сети Вконтакте](https://vk.com/), плейлист которого составляется из аудиозаписей со стен подписок пользователя (групп/пабликов).

### Goodnight, sweet prince ###

С ноября 2016 ВКонтакте отказался от использования scope = nohttps. Уже была начата работа по доработке модуля VK.pbi и Network.pbi для работы с libcurl, как стало известно, что с 16 декабря 2016 года ВКонтакте отключает публичный API для работы с аудиозаписями. Так что этот проект, в данный момент времени, представляет исключительно историческую ценность.

###  Авторства ###

Функции плеера выполняет [BASS audio library](http://www.un4seen.com/).
Copyright © 2003-2015 un4seen developments

Использованы иконки из набора [Breeze](https://github.com/NitruxSA/breeze-icon-theme).

Изображение лимандарина взято [здесь](https://goo.gl/5ytg4O).

Программа была разработана в [PureBasic 5.31](http://www.purebasic.com).
Copyright © 2014 Fantaisie Software

Для парсинга XML задействован [Expat 2.0.1](http://expat.sourceforge.net/).
Copyright © 1998 - 2000 Thai Open Source Software Center Ltd and Clark Cooper
Copyright © 2001 - 2006 Expat maintainers.

###  Лицензия ###

Распространяется по лицензии [Experimental Software License (ESL)](http://lhs-blog.info/info/litsenziya-esl-dlya-programmnogo-obespecheniya/).

###  Обратная связь ###

* ragweedsplashing@mail.ru

Copyright © 2015-2016 ragweed & LightHouse Software.
[http://lhs-blog.info/](http://lhs-blog.info/)