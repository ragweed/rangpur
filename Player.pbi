﻿
DeclareModule Player
  Declare Init(windows_id, volume)
  Declare Open(url.s)
  Declare Play()
  Declare Pause()
  Declare Stop()
  Declare IsPlayed()
  Declare SetVolume(volume)
  Declare GetPosition()
  Declare GetLength()
  Declare SetPosition(position)
  Declare IsInitStream()
  Declare GetPercentageOfBufferFilled()
EndDeclareModule

Module Player
  
  ; BASS LIB наше всё!
  ; Таки BASS 2.4
  ; Copyright (c) 1999-2014 Un4seen Developments Ltd. All rights reserved.
  IncludeFile "bass.pbi"

  ; Здесь будем хранить хэндл потока, в котором идёт открытие аудиопотока.
  Global WaitingOpenStream
  
  ; А здесь будет хранится хэндл текущего аудиопотока (объяснил, как отрезал, блин).
  Global StreamHandle
  
  ; А здесь будет хранится URL адрес текущего аудиопотока.
  Global CurrentStreamURL.s
  
  ; Эта переменная равна 1, если поток восроизводится, такие дела.
  Global StreamPlaying
  
  ; Переменная равна 1, пока идёт ожидания начала воспроизведения потока.
  Global InitStream
  
  ; Через эти переменные отслеживается прогресс загрузки аудиопотока в память, всё ради GetPercentageOfBufferFilled().
  Global CurrentStreamLengthDownload, CurrentStreamLoaded
  
  ; Данная переменная хранит текущий уровень громкости.
  Global CurrentVolume.d = 1
  
  ; Инициализация BASS LIB.
  Procedure Init(windows_id, volume)
    BASS_Init(-1, 44100, WindowID(windows_id), 0, #Null)
    CurrentVolume.d = volume/100
  EndProcedure
  
  ; Callback для BASS_StreamCreateURL(), всё ради GetPercentageOfBufferFilled().
  ;{ Весело тут...
  
  ProcedureC LinStreamCallback(*buffer, length.i, *user)
    If Not *buffer
      CurrentStreamLoaded = 1
    Else
      CurrentStreamLengthDownload + length
    EndIf
  EndProcedure
  
  Procedure WinStreamCallback(*buffer, length.i, *user)
    If Not *buffer
      CurrentStreamLoaded = 1
    Else
      CurrentStreamLengthDownload + length
    EndIf
  EndProcedure
  
  Procedure StreamCallback() ; Вернёт указатель на нужный Callback.
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
        ProcedureReturn @WinStreamCallback()
      CompilerCase #PB_OS_Linux
        ProcedureReturn @LinStreamCallback()
    CompilerEndSelect
  EndProcedure
  
  ;}
  
  Procedure OpenStreamForThread(null)
    NewStreamHandle = BASS_StreamCreateURL(CurrentStreamURL.s, 0, 0, StreamCallback(), 0)
    BASS_StreamFree(StreamHandle)
    StreamPlaying = 0
    StreamHandle = NewStreamHandle
    If StreamHandle <> 0
      BASS_ChannelSlideAttribute(StreamHandle, #BASS_ATTRIB_VOL, CurrentVolume, 0)
      BASS_ChannelPlay(StreamHandle, 0)
      CurrentStreamLengthDownload = 0
      StreamPlaying = 1
    Else
      Debug "OpenStreamForThread() BASS Error " + Str(BASS_ErrorGetCode())
    EndIf
    InitStream = 0
  EndProcedure
  
  ; Изменяет текущий url.s аудиопотока.
  Procedure SetCurrentStreamURL(url.s)
    CurrentStreamURL.s = url.s
  EndProcedure  
    
  ; Открытие и начало воспроизведения аудиопотока url.s.
  Procedure Open(url.s)
    SetCurrentStreamURL(url.s)
    InitStream = 1
    CurrentStreamLoaded = 0
    BASS_StreamFree(StreamHandle)
    WaitingOpenStream = CreateThread(@OpenStreamForThread(), #Null)
  EndProcedure

  ; Воспроизведения аудиопотока, созданного Open(url.s). Вернёт 1 в случае успеха.
  Procedure Play()
    ; А был ли открыт аудиопоток?
    If CurrentStreamURL<>""
      ; А вдруг поток был остановлен Stop().
      If StreamHandle=0
        Open(CurrentStreamURL.s)
        result = 1
      Else
        BASS_ChannelPlay(StreamHandle,0)
        StreamPlaying = 1
        result = 1
      EndIf
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Приостанавливает воспроизведения аудиопотока, созданного Open(url.s).
  Procedure Pause()
    BASS_ChannelPause(StreamHandle)
    StreamPlaying = 0
  EndProcedure
  
  ; Останавливает воспроизведения аудиопотока, созданного Open(url.s).
  Procedure Stop()
    BASS_StreamFree(StreamHandle) ; Затираем нафиг старый аудиопотока.
    StreamPlaying = 0
    StreamHandle = 0
  EndProcedure
  
  ; Возращает 1, если идёт инициализация воспроизведения потока.
  Procedure IsInitStream()
    ProcedureReturn InitStream
  EndProcedure
  
  ; Вернёт 0, если аудиопоток невосроизводится при StreamPlaying = 1.
  ; Необходима для реализации автоматического переключения треков в плейлисте.
  Procedure IsPlayed()
    If InitStream = 0
      If StreamPlaying = 1
        If BASS_ChannelIsActive(StreamHandle)
          result = 1
        Else
          result = 0
          StreamPlaying = 0
        EndIf
      Else
        result = -1
      EndIf
    Else
      result = -1
    EndIf
    ProcedureReturn result
  EndProcedure  
  
  ; Изменяет громкость воспроизведения аудиоптока. Зн. volume = 0 - мин. громкость, 100 - макс.
  Procedure SetVolume(volume)
    CurrentVolume = volume/100
    BASS_ChannelSlideAttribute(StreamHandle, #BASS_ATTRIB_VOL, CurrentVolume, 500)
  EndProcedure
  
  ; Вернёт позицию воспроизведения в килобайтах.
  Procedure GetPosition()
    position = BASS_ChannelGetPosition(StreamHandle, #BASS_POS_BYTE)
    ProcedureReturn position/1024
  EndProcedure
  
  ; Переместит позицию воспроизведения. position указыватся в килобайтах.
  Procedure SetPosition(position)
    result = BASS_ChannelSetPosition(StreamHandle, position*1024, #BASS_POS_BYTE)
    ProcedureReturn result
  EndProcedure
  
  ; Вернёт размер воспроизводимого аудиофайла в килобайтах.
  Procedure GetLength()
    lenght = BASS_ChannelGetLength(StreamHandle, #BASS_POS_BYTE)
    ProcedureReturn lenght/1024
  EndProcedure
  
  ; Вернёт наполнение буфера в процентах.
  Procedure GetPercentageOfBufferFilled()
    ; Должно быть так...
    ;len = BASS_StreamGetFilePosition(stream, BASS_FILEPOS_END)    ; длинна файла/буфера.
    ;buf = BASS_StreamGetFilePosition(stream, BASS_FILEPOS_BUFFER) ; наполнение буфера.
    ;progress = buf*100/len                                        ; наполнение буфера в процентах.
    ; Но...
    If CurrentStreamLoaded = 1
      result = 100
    Else
      result = ((CurrentStreamLengthDownload*4.7)/1024)*100/GetLength()
      If result > 99
        result = 99
      EndIf
    EndIf
    ProcedureReturn result
  EndProcedure

EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 106
; FirstLine = 87
; Folding = ----
; EnableUnicode
; EnableXP