﻿DeclareModule GUI
  
  Enumeration FormWindow
    #Window_General
    #Window_Settings
    #Window_SelectAlbums
    #Window_EditSubscribes
  EndEnumeration
  
  Enumeration FormGadget
    #GW_Menu_Others
    #GW_Fun_Update
    #GW_Fun_MarkPlayed
    #GW_Fun_Settings
    #GW_Fun_Exit

    #GW_Container_Toolbar
    
    #GW_ButtonImage_Previous
    #GW_ButtonImage_PlayAndPause
    #GW_ButtonImage_Next
    #GW_ButtonImage_Stop
    #GW_TrackBar_TrackScroll
    #GW_TrackBar_Volume
    #GW_ButtonImage_Menu
    #GW_ProgressBar_TrackLoadProgress
    
    #GW_Container_Playlist
    
    #GW_ListIcon_Playlist
    #GW_Button_FeedMusic
    #GW_Button_VKAlbums
    
    #GW_Container_Albums
    
    #GW_ListView_Albums
    
    #GW_Container_Others
    
    #GW_WebView_Artwork
    #GW_Button_OpenPost
    #GW_Button_AddInMyAudios
    #GW_Button_SkipPost
    #GW_Button_MarkAllPlayed
    
    #SW_Panel
    #SW_Button_EditSubsList
    #SW_WebView_UserAvatar
    #SW_Text_UserName
    #SW_Text_CountSubscribes
    #SW_Option_WebBrowser_Default
    #SW_Frame_WebBrowser
    #SW_String_Path_WebBrowser
    #SW_Button_Path_WebBrowser
    #SW_Button_SaveOthers
    #SW_Option_WebBrowser_Manual
    #SW_Option_Artwork_Default
    #SW_Frame_Artwork
    #SW_String_Path_Artwork
    #SW_Button_Path_Artwork
    #SW_Option_Artwork_Manual
    #SW_Checkbox_DisableAlbumsWindow
    #SW_Checkbox_AutoLikePost
    #SW_Frame_AddInMyAudios
    #SW_Button_ClearDB
    #SW_Button_ReCreatedDB
    
    #SAW_ListView_AlbumsList
    #SAW_Button_Save
    #SAW_Button_Cancel
    #SAW_Text_Info
    
    #ESW_Frame_Subscribes
    #ESW_ListIcon_Subscribes
    #ESW_Button_Save
    #ESW_Button_Cancel
  EndEnumeration

  Enumeration TypeOfPlaylist
    #Playlist_UnPlayed
    #Playlist_All
  EndEnumeration
  
  Enumeration PlayerMode
    #Player_Feed
    #Player_VKMusic
  EndEnumeration
  
  Enumeration FormImage
    #Img_Play
    #Img_Current
    #Img_Pause
    #Img_Previous
    #Img_Next
    #Img_Menu
    #Img_Stop
  EndEnumeration
  
  Declare OpenWindow_General()
  Declare OpenWindow_Settings()
  Declare OpenWindow_SelectAlbums()
  Declare OpenWindow_EditSubscribes()

  Declare SetStatusBarText(text.s)
  Declare SetStatusBarProgress(value = 0, max = 0)
  Declare SetImageInWebGadget(wg_id, image_url.s, text.s="")
  Declare SetPostInfo(text.s, image_url.s="")
  Declare SwitchPlayerMode(mode)
  
  ; Текущий режим работы плеера хранится в этой переменной.
  Global PlayerMode.l
  
  ; Если DefaultArtwork равен 1, то Rangpur будет использовать в качестве обложки по-умолчанию лимандарин.
  ; Иначе будет использоватся изображение, расположенное в ArtworkPath.
  Global DefaultArtwork.a, ArtworkPath.s

EndDeclareModule

Module GUI

  UsePNGImageDecoder()

  LoadImage(#Img_Play,"icons/play.png")
  LoadImage(#Img_Pause,"icons/pause.png")
  LoadImage(#Img_Previous,"icons/previous.png")
  LoadImage(#Img_Next,"icons/next.png")
  LoadImage(#Img_Menu,"icons/menu.png")
  LoadImage(#Img_Stop,"icons/stop.png")
  LoadImage(#Img_Current,"icons/current.png")
  
  ; Изменяет текст в статусбаре.
  Procedure SetStatusBarText(text.s)
    StatusBarText(0, 0, text.s, #PB_StatusBar_Raised)
  EndProcedure

  ; Изменяет прогресс в статусбаре.
  Procedure SetStatusBarProgress(value = 0, max = 0)
    If value <> 0 And max <> 0
      StatusBarProgress(0, 1, value, #PB_StatusBar_Raised, 0 , max)
    Else
      ;- Интересно, а можно ли спрятать прогрессбар?
    EndIf
  EndProcedure
  
  ; Изменит изображение отображаемое в WebGadget.
  Procedure SetImageInWebGadget(wg_id, image_url.s, text.s="")
    If #PB_Compiler_OS = #PB_OS_Windows
      SetGadgetItemText(wg_id, #PB_Web_HtmlCode, 
                              "<img alt='"+text.s+"' width='100%' src='"+image_url.s+"'>")
    Else
      SetGadgetText(wg_id, image_url.s)
    EndIf
  EndProcedure
  
  ; Отобразит информацию о посте.
  Procedure SetPostInfo(text.s, image_url.s="")
    If image_url.s=""
      If DefaultArtwork.a = 0
        image_path.s = ArtworkPath.s
      Else
        image_path.s = GetCurrentDirectory()+"/artworks/rangpur.jpg"
      EndIf
      image_url.s = "file:///"+image_path.s
    EndIf
    SetImageInWebGadget(#GW_WebView_Artwork, image_url.s, text.s)
  EndProcedure
  
  ; ВНЕЗАПНО, выводит окно плеера на экран.
  Procedure OpenWindow_General()
    
    OpenWindow(#Window_General, #PB_Ignore, #PB_Ignore, 640, 435, "Rangpur", #PB_Window_MinimizeGadget | #PB_Window_SystemMenu | #PB_Window_ScreenCentered )
    SmartWindowRefresh(#Window_General, 1)

    ;{ Menu_Others
    If CreatePopupMenu(#GW_Menu_Others)
      MenuItem(#GW_Fun_Update, "Проверить новые аудиозаписи")
      MenuItem(#GW_Fun_MarkPlayed, "Отметить все композиции как прослушанные")
      MenuBar()
      MenuItem(#GW_Fun_Settings, "Настройки")
      MenuBar()
      MenuItem(#GW_Fun_Exit, "Выход"+Chr(9)+"Alt+F4")
    EndIf
    ;}
    
    ;{ StatusBar
    CreateStatusBar(0, WindowID(#Window_General))
    AddStatusBarField(#PB_Ignore)
    AddStatusBarField(100)
    StatusBarText(0, 0, "Готово.")
    
    
    ResizeWindow(#Window_General, #PB_Ignore, #PB_Ignore, 
                 WindowWidth(#Window_General), WindowHeight(#Window_General) + StatusBarHeight(0))
    ;}
    
    ;{ Container_Toolbar
    ContainerGadget(#GW_Container_Toolbar, 0, 0, 640, 40)
    
    ButtonImageGadget(#GW_ButtonImage_Previous, 10, 5, 30, 30, ImageID(#Img_Previous))
    ButtonImageGadget(#GW_ButtonImage_PlayAndPause, 40, 5, 30, 30, ImageID(#Img_Play), #PB_Button_Toggle)
    SetGadgetAttribute(#GW_ButtonImage_PlayAndPause, #PB_Button_PressedImage, ImageID(#Img_Pause))
    ButtonImageGadget(#GW_ButtonImage_Next, 70, 5, 30, 30, ImageID(#Img_Next))
    ButtonImageGadget(#GW_ButtonImage_Stop, 100, 5, 30, 30, ImageID(#Img_Stop))
    
    If #PB_Compiler_OS = #PB_OS_Windows
      TrackBarGadget(#GW_TrackBar_TrackScroll, 135, 15, 350, 20, 0, 0)
      ProgressBarGadget(#GW_ProgressBar_TrackLoadProgress, 142, 10, 336, 5, 0, 0)
      TrackBarGadget(#GW_TrackBar_Volume, 485, 10, 110, 20, 0, 0)
    Else
      ; Йааааааа... А чтоб понять в чём соль - можно здесь вот почитать:
      ; http://purebasic.info/phpBB3ex/viewtopic.php?f=14&t=4260
      ContainerGadget(#PB_Any, 135, 10, 345, 20)
      TrackBarGadget(#GW_TrackBar_TrackScroll, 0, 0, 345, 30, 0, 0)
      CloseGadgetList()
      ProgressBarGadget(#GW_ProgressBar_TrackLoadProgress, 138, 7, 340, 4, 0, 0)
      ContainerGadget(#PB_Any, 490, 10, 100, 20)
      TrackBarGadget(#GW_TrackBar_Volume, 0, 0, 100, 30, 0, 0)
      CloseGadgetList()
    EndIf

    SetGadgetAttribute(#GW_ProgressBar_TrackLoadProgress, #PB_ProgressBar_Maximum, 100)
    SetGadgetAttribute(#GW_TrackBar_Volume, #PB_TrackBar_Maximum, 100)
    ButtonImageGadget(#GW_ButtonImage_Menu, 600, 5, 30, 30, ImageID(#Img_Menu))

    CloseGadgetList()
    ;}
    
    ;{ Container_Playlist
    ContainerGadget(#GW_Container_Playlist, 0, GadgetHeight(#GW_Container_Toolbar), 640, 400)
    
    ButtonGadget(#GW_Button_FeedMusic, 10, 0, 220, 25, "Не прослушанное", #PB_Button_Toggle)
    ButtonGadget(#GW_Button_VKAlbums, 10, 25, 220, 25, "ВК альбомы", #PB_Button_Toggle)
    
    ;{ -- Container_Albums
    ContainerGadget(#GW_Container_Albums, 10, 50, 225, 340)
    ListViewGadget(#GW_ListView_Albums, 0, 5, 220, 335)
    CloseGadgetList()
    HideGadget(#GW_Container_Albums, 1)
    ;}
    
    ;{ -- Container_Others
    ContainerGadget(#GW_Container_Others, 10, 50, 225, 340)

    If #PB_Compiler_OS = #PB_OS_Windows
      ContainerGadget(#PB_Any, 0, 5, 220, 220)
      WebGadget(#GW_WebView_Artwork, -15, -15, 265, 250, "")
      CloseGadgetList()
    Else
      If Not WebGadget(#GW_WebView_Artwork, 0, 5, 220, 220, "")
        MessageRequester("Rangpur", "Отсутствует библиотека libwebkit-dev."+Chr(13)+"Пользователи Debian-based дистрибутивов могут исправить положение,"+Chr(13)+"выполнив в терминале: sudo apt-get install libwebkit-dev")
        End
      EndIf
    EndIf

    SetPostInfo("", "")
    
    ButtonGadget(#GW_Button_OpenPost, 0, 280, 220, 25, "Открыть пост")
    ButtonGadget(#GW_Button_AddInMyAudios, 0, 230, 220, 25, "Добавить в аудиозаписи")
    ButtonGadget(#GW_Button_SkipPost, 0, 255, 220, 25, "Пропустить треки этого поста")
    ButtonGadget(#GW_Button_MarkAllPlayed, 0, 315, 220, 25, "Отметить все как прослушанное")
    FrameGadget(#PB_Any, 0, 305, 220, 5, "")
    
    CloseGadgetList()
    HideGadget(#GW_Container_Others, 1)
    ;}

    ListIconGadget(#GW_ListIcon_Playlist, 240, 0, 390, 390, "#", 25, #PB_ListIcon_FullRowSelect | #PB_ListIcon_AlwaysShowSelection)
    AddGadgetColumn(#GW_ListIcon_Playlist, 1, "Автор", 120)
    AddGadgetColumn(#GW_ListIcon_Playlist, 2, "Название", 180)
    AddGadgetColumn(#GW_ListIcon_Playlist, 3, "Прод.", 50)
    
    CloseGadgetList()
    ;}

  EndProcedure
  
  ; Если mode=#Player_Feed скроет список вконтактовских альбомов и отобразит кнопки для взаимодействия
  ; с композициями из списка непрослушанных (собранных со стен групп), и наоборот для mode=#Player_VKMusic
  Procedure SwitchPlayerMode(mode)
    If mode=#Player_Feed
      PlayerMode = #Player_Feed
      
      HideGadget(#GW_Container_Albums, 1)
      HideGadget(#GW_Container_Others, 0)
      
      SetGadgetState(#GW_Button_FeedMusic, 1)
      SetGadgetState(#GW_Button_VKAlbums, 0)
      
      DisableGadget(#GW_Button_FeedMusic, 1)
      DisableGadget(#GW_Button_VKAlbums, 0)
    Else
      PlayerMode = #Player_VKMusic
      
      HideGadget(#GW_Container_Albums, 0)
      HideGadget(#GW_Container_Others, 1)
      
      SetGadgetState(#GW_Button_FeedMusic, 0)
      SetGadgetState(#GW_Button_VKAlbums, 1)
      
      DisableGadget(#GW_Button_FeedMusic, 0)
      DisableGadget(#GW_Button_VKAlbums, 1)
    EndIf
  EndProcedure
  
  ; Не менее внезапно отобразит окно настроек на экран.
  Procedure OpenWindow_Settings()
    font_big = LoadFont(#PB_Any, "Arial", 20)
    
    OpenWindow(#Window_Settings, #PB_Ignore, #PB_Ignore, 605, 350, "Настройка Rangpur", #PB_Window_Invisible | #PB_Window_SystemMenu | #PB_Window_ScreenCentered | #PB_Window_WindowCentered)
    PanelGadget(#SW_Panel, 6, 5, 595, 340)
    AddGadgetItem(#SW_Panel, -1, "ВК Аккаунт")
    If #PB_Compiler_OS = #PB_OS_Windows
      ContainerGadget(#PB_Any, 85, 68, 150, 150)
      WebGadget(#SW_WebView_UserAvatar, -15, -15, 195, 180, "")
      CloseGadgetList()
    Else
      WebGadget(#SW_WebView_UserAvatar, 85, 68, 150, 150, "")
    EndIf
    TextGadget(#SW_Text_UserName, 245, 73, 330, 75, "")
    SetGadgetFont(#SW_Text_UserName, FontID(font_big))
    TextGadget(#PB_Any, 245, 153, 330, 25, "Количество подписок, за которыми следит Rangpur:")
    TextGadget(#SW_Text_CountSubscribes, 245, 178, 35, 35, "16", #PB_Text_Center)
    SetGadgetFont(#SW_Text_CountSubscribes, FontID(font_big))
    ButtonGadget(#SW_Button_EditSubsList, 285, 178, 130, 35, "Изменить", #PB_Button_Default)
    FrameGadget(#PB_Any, 65, 248, 455, 55, "ATTENTION! Опасная зона!")
    ButtonGadget(#SW_Button_ClearDB, 85, 268, 200, 25, "Очистить базу данных")
    ButtonGadget(#SW_Button_ReCreatedDB, 300, 268, 200, 25, "Пересоздать базу данных")
    AddGadgetItem(#SW_Panel, -1, "Прочее")
    OptionGadget(#SW_Option_WebBrowser_Default, 30, 123, 515, 25, "Использовать браузер назначенный по-умолчанию")
    OptionGadget(#SW_Option_WebBrowser_Manual, 30, 148, 515, 25, "Использовать другой браузер:")
    SetGadgetState(#SW_Option_WebBrowser_Default, 1)
    FrameGadget(#SW_Frame_WebBrowser, 15, 108, 560, 100, "Веб-браузер")
    StringGadget(#SW_String_Path_WebBrowser, 30, 173, 420, 25, "vivaldi")
    ButtonGadget(#SW_Button_Path_WebBrowser, 455, 173, 105, 25, "Обзор")
    OptionGadget(#SW_Option_Artwork_Default, 30, 23, 515, 25, "Лимандарин (rangpur)")
    OptionGadget(#SW_Option_Artwork_Manual, 30, 48, 515, 25, "Использовать другое изображение:")
    FrameGadget(#SW_Frame_Artwork, 15, 8, 560, 100, "Обложка по-умолчанию")
    StringGadget(#SW_String_Path_Artwork, 30, 73, 420, 25, "")
    ButtonGadget(#SW_Button_Path_Artwork, 455, 73, 105, 25, "Обзор")
    CheckBoxGadget(#SW_Checkbox_DisableAlbumsWindow, 30, 223, 515, 25, "Добавлять композиции сразу в 'Мои аудиозаписи', не отображая список альбомов")
    CheckBoxGadget(#SW_Checkbox_AutoLikePost, 30, 248, 515, 25, "'Лайкать' пост добавляемой композиции")
    FrameGadget(#SW_Frame_AddInMyAudios, 15, 208, 560, 70, "Поведение кнопки 'Добавить в аудиозаписи'")
    ButtonGadget(#SW_Button_SaveOthers, 450, 283, 125, 25, "Сохранить", #PB_Button_Default)
    CloseGadgetList()
    HideWindow(#Window_Settings, 0)
  EndProcedure
  
  ; Отобразит окно 'выбора альбома' на экран.
  Procedure OpenWindow_SelectAlbums()
    OpenWindow(#Window_SelectAlbums, #PB_Ignore, #PB_Ignore, 215, 320, "Выберите альбом для сохранения", #PB_Window_WindowCentered)
    ListViewGadget(#SAW_ListView_AlbumsList, 5, 45, 205, 240)
    ButtonGadget(#SAW_Button_Save, 5, 290, 100, 25, "Сохранить")
    ButtonGadget(#SAW_Button_Cancel, 110, 290, 100, 25, "Отмена")
    TextGadget(#SAW_Text_Info, 5, 5, 205, 40, "Пожайлуста, подождите. Загружается список альбомов...", #PB_Text_Center)
  EndProcedure
  
  ; Отобразит окно выбора подписок на экран.
  Procedure OpenWindow_EditSubscribes()
    OpenWindow(#Window_EditSubscribes, #PB_Ignore, #PB_Ignore, 320, 365, "", #PB_Window_WindowCentered)
    FrameGadget(#ESW_Frame_Subscribes, 5, 3, 310, 327, "Отметьте галочкой подписки с музыкой")
    ListIconGadget(#ESW_ListIcon_Subscribes, 10, 23, 300, 300, "#", 30, #PB_ListIcon_CheckBoxes | #PB_ListIcon_FullRowSelect | #PB_ListIcon_AlwaysShowSelection)
    AddGadgetColumn(#ESW_ListIcon_Subscribes, 1, "Название", 240)
    ButtonGadget(#ESW_Button_Save, 5, 333, 195, 25, "Сохранить изменения")
    ButtonGadget(#ESW_Button_Cancel, 210, 333, 100, 25, "Отмена")
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 187
; FirstLine = 173
; Folding = -4-
; EnableUnicode
; EnableXP