﻿
DeclareModule UpdateMusicList
  
  ; Структура для Start(), в которой будет фиксироватся информация о прогрессе обновления аудиозаписей.
  Structure Info
    is.a
    subscribe.s
    stage.l
    count.l
  EndStructure
  Global Current.Info
  
  Declare Start()
  
  ; Если будет равна 1, Start() прекратит свою работу.
  Global StopUpdateList
  
EndDeclareModule

Module UpdateMusicList
  
  ; Вспомогательная процедура для ParsePostsXML().
  ; Не забывай, что функция не работает с xml-деревом полученным напрямую процедурой VK::GetNewPosts(), 
  ; а с узлом 'attachments', перенесённым из /response/items/item[]/attachments/ в /response/attachments/.
  Procedure ParseAudiosFromAttachsXML(xml.s, info_id, subscribe_id, post_id, List TempAudioList.s())
    If xml.s<>""
      NewList Requests.s()
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment/")
      If *CurrentNode
        num_attach=1
        While *CurrentNode
          *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/type/")
          attach_type.s = GetXMLNodeText(*TempNode)
       
          If attach_type.s = "audio"
            ; https://vk.com/dev/audio_object
            
            *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/audio/id/")
            
            If *TempNode ; Если <audio></audio> не пуст...
              audio_id = Val( GetXMLNodeText(*TempNode) )
              *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/audio/owner_id/")
              owner_id = Val( GetXMLNodeText(*TempNode) )
              *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/audio/genre_id/")
              
              If *TempNode
                genre_id = Val( GetXMLNodeText(*TempNode) )
              EndIf  
                
              *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/audio/duration/")
              duration = Val(GetXMLNodeText(*TempNode))
              *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/audio/artist/")
              artist.s = GetXMLNodeText(*TempNode)
              *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/audio/title/")
              title.s = GetXMLNodeText(*TempNode)

              AddElement(TempAudioList.s())
              TempAudioList() = DB::ReturnAddAudioQueryString(audio_id, owner_id, artist.s, title.s, duration, genre_id, subscribe_id, post_id, info_id)
              audio_count + 1
            EndIf
          EndIf 
              
          num_attach=num_attach+1
          *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/")
        Wend 
          
      EndIf
    EndIf
    
    ProcedureReturn audio_count
  EndProcedure
  
  ; Вспомогательная процедура для ParsePostsXML().
  ; Не забывай, что функция не работает с xml-деревом полученным напрямую процедурой VK::GetNewPosts(), 
  ; а с узлом 'attachments', перенесённым из /response/items/item[]/attachments/ в /response/attachments/.
  Procedure ParseFhotoFromAttachsXML(xml.s, num_attach, *photo.DB::Info)
    ;https://vk.com/dev/photo
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/photo/")
      If *CurrentNode

        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/photo/photo_75/")
        If *TempNode 
          *photo\photo_75 = GetXMLNodeText(*TempNode)
        Else
          *photo\photo_75 = ""
        EndIf
        
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/photo/photo_130/")
        If *TempNode 
          *photo\photo_130 = GetXMLNodeText(*TempNode)
        Else
          *photo\photo_130 = ""
        EndIf
        
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/photo/photo_604/")
        If *TempNode 
          *photo\photo_604 = GetXMLNodeText(*TempNode)
        Else
          *photo\photo_604 = ""
        EndIf
        
      Else
        *photo\photo_75  = ""
        *photo\photo_130 = ""
        *photo\photo_604 = ""
      EndIf
    EndIf
  EndProcedure
  
  ; Вспомогательная процедура для ParsePostsXML().
  ; Вернёт 1, если будут найдены приаттаченные аудиофайлы в xml.s.
  ; Не забывай, что функция не работает с xml-деревом полученным напрямую процедурой VK::GetNewPosts(), 
  ; а с узлом 'attachments', перенесённым из /response/items/item[]/attachments/ в /response/attachments/.
  Procedure IsAudio(xml.s)
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment/")
      If *CurrentNode
        num_attach=1
        While *CurrentNode
          *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/type/")
          attach_type.s = GetXMLNodeText(*TempNode)
       
          If attach_type.s = "audio"
            result = 1
            Break
          EndIf 
              
          num_attach=num_attach+1
          *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/")
        Wend
      EndIf
    EndIf
    
    ProcedureReturn result
  EndProcedure
  
  ; Вспомогательная процедура для ParsePostsXML().
  ; Вернёт номер attachment, если будут найдены приаттаченные изображения в xml.s. Иначе 0.
  ; Не забывай, что функция не работает с xml-деревом полученным напрямую процедурой VK::GetNewPosts(), 
  ; а с узлом 'attachments', перенесённым из /response/items/item[]/attachments/ в /response/attachments/.
  Procedure IsPhoto(xml.s)
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment/")
      If *CurrentNode
        num_attach=1
        While *CurrentNode
          *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/type/")
          attach_type.s = GetXMLNodeText(*TempNode)
       
          If attach_type.s = "photo"
            result = num_attach
            Break
          EndIf 
              
          num_attach=num_attach+1
          *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/attachments/attachment["+Str(num_attach)+"]/")
        Wend
      EndIf
    EndIf
    
    ProcedureReturn result
  EndProcedure
  
  ; Возращает xml-узел *Node в виде самостоятельного xml-дерева.
  ; Данная процедура необходима для формирования xml-дерева аттачей для IsAudio() и ParseAndSaveAudiosFromAttachsXML().
  Procedure.s GetXML(*Node)
    If *Node
      temp_xml_id = CreateXML(#PB_Any) 
      *temp_xml_root = CreateXMLNode(RootXMLNode(temp_xml_id), "response")
      *temp_xml_attachments = CopyXMLNode(*Node, *temp_xml_root)
      FormatXML(temp_xml_id,#PB_XML_ReFormat)
      *MemoryID = AllocateMemory(ExportXMLSize(temp_xml_id)+1)
      ExportXML(temp_xml_id, *MemoryID, MemorySize(*MemoryID))
      xml.s = PeekS(*MemoryID, MemorySize(*MemoryID), #PB_UTF8)
      FreeXML(temp_xml_id)
    EndIf
    ProcedureReturn xml.s
  EndProcedure
  
  ; Парсит XML дерево с вк постами на предмет наличия аудиозаписей.
  ; Возращает дату последнего поста.
  Procedure ParsePostsXML(xml.s)
    If xml.s<>""
      ; Для артворков структуру сразу создадим, чтобы было.
      photos.DB::Info
      ; Я пишу такие информативные и лаконичные комментарии, что лучше бы их не писать вовсе :(
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item/")
      num_post=1
      
      ; Все аудиозаписи, найденные в процессе парсинга будет здесь!
      NewList TempAudioList.s()
      
      ; Перебираем посты в цикле...
      While *CurrentNode
        
        ; last_date.
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/date/")
        
        If Val( GetXMLNodeText(*TempNode) ) > last_date
          last_date = Val( GetXMLNodeText(*TempNode) )
        EndIf
        
        ; subscribe_id.
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/source_id/")
        subscribe_id = Val( GetXMLNodeText(*TempNode) )

        ;{ Проверим, а есть ли аудиозаписи в посте?

        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/attachments/")
        attachments.s=GetXML(*TempNode)
        If IsAudio(attachments.s) <> 0 
          ; Таки есть!
          ; Формируем информацию о треке исходя из содержимого поста.
          *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/post_id/")
          post_id = Val( GetXMLNodeText(*TempNode) )
          *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/text/")
          text.s = GetXMLNodeText(*TempNode)
          
          num_attachment = IsPhoto(attachments.s)
          If num_attachment <> 0
            ParseFhotoFromAttachsXML(attachments.s, num_attachment, photos)
          Else
            ClearStructure(photos, DB::Info)
          EndIf

          info_id = DB::AddInfo(subscribe_id, post_id, text.s, photos)
          ParseAudiosFromAttachsXML(attachments.s, info_id, subscribe_id, post_id, TempAudioList.s())
        EndIf  
        
        ;}
        
        ;{ А есть ли аудиозаписи в прирепостенной части?
        
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/copy_history/")
        If *TempNode
          *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/copy_history/post/attachments/")
          repost_attachments.s=GetXML(*TempNode)
          If IsAudio(repost_attachments.s)>0
            ; Таки есть!
            ; Формируем информацию о треке исходя из содержимого поста.
            ; В будущем осилим формирование артворка из приатаченных изображений и может ещё чего... ^_^
            *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/copy_history/post/id/")
            repost_id = Val( GetXMLNodeText(*TempNode) )
            *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/copy_history/post/owner_id/")
            repost_owner_id = Val( GetXMLNodeText(*TempNode) )
            *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/copy_history/post/from_id/")
            repost_from_id = Val( GetXMLNodeText(*TempNode) )
            *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/copy_history/post/text/")
            repost_text.s = GetXMLNodeText(*TempNode)
            
            num_attachment = IsPhoto(repost_attachments.s)
            If num_attachment <> 0
              ParseFhotoFromAttachsXML(repost_attachments.s, num_attachment, photos)
            Else
              ClearStructure(photos, DB::Info)
            EndIf
            
            info_id = DB::AddInfo(repost_owner_id, repost_id, repost_text.s, photos, subscribe_id)
            ParseAudiosFromAttachsXML(attachments.s, info_id, repost_owner_id, repost_id, TempAudioList.s())
          EndIf
        EndIf
        
        ;}
        
        num_post=num_post+1
        *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/item["+Str(num_post)+"]/")
      Wend
      
      If ListSize(TempAudioList.s()) > 0 
        DB::QueriesInTheTransaction(TempAudioList.s())
      EndIf 
      
      FreeXML(xml_id)
      ProcedureReturn last_date
    EndIf
  EndProcedure
  
  ; Ищет и сохраняет новые аудиозаписи в подписке за промежуток времени с last_date по текущий момент.
  ; Возращает дату последнего поста в подписке.
  Procedure SearchNewMusicFromSubscribe(subscribe_id, last_date)

    ; Запрашиваем у VK новые посты, в которых будем искать аудиозаписи.
    xml.s = VK::GetNewPosts(subscribe_id, last_date)
    
    If xml.s <> ""
      ; Парсим XML.
      last_date = ParsePostsXML(xml.s)
    Else
      StopUpdateList = 1
    EndIf  
      
    ProcedureReturn last_date
    
  EndProcedure
  
  ; Проверяет подписки на наличие новых аудиозаписей и сохраняет их в БД.
  Procedure Start()
    
    StopUpdateList = 0
    Current\is = 1
    
    ; Получаем список подписок.
    sub_count = DB::GetSubscribesList(DB::SubscribesList.DB::Subscribe())
    Current\count = sub_count
    
    ; В цикле перебираем subscribe_id.
    ForEach DB::SubscribesList()
      num + 1
      name.s = DB::SubscribesList()\name
      sub_id = DB::SubscribesList()\subscribe_id
      ;Debug "Ищу музыку в "+name.s+" ["+Str(num)+" из "+Str(sub_count)+"]"
      Current\subscribe = name.s
      Current\stage = num
      
      ; Ищем и сохраняем новые аудиозаписи в subscribe_id.
      ; К дате последнего поста прибавлю секунду чтобы исключить получение и парсинг этого самого поста, 
      ; а, следовательно, и дублирование аудиозаписей. Вот.
      current_last_date = DB::GetLastDateFromSubscribe(sub_id)+1
      new_last_date = SearchNewMusicFromSubscribe(sub_id, current_last_date)
      
      ; Через эту переменную пользователь может остановить сканирование.
      If StopUpdateList = 1
        Break
      EndIf
      
      If new_last_date>current_last_date
        DB::SetLastDateFromSubscribe(sub_id, new_last_date)
      EndIf
    Next

    Current\is = 0
    
  EndProcedure
  
EndModule  

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 326
; Folding = ---
; EnableUnicode
; EnableXP