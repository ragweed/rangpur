﻿
DeclareModule Action
  
  Enumeration PlayType
    #Previous
    #Current
    #Next
  EndEnumeration
  
  ; General
  Declare SwitchPlayerMode(player_mode)
  Declare PasteMusicList(list_type=DB::#UnPlayed)
  Declare PasteVKMusicList(album_id=VK::#FakeAlbum_All)
  Declare PasteAlbumsList(lw_id = GUI::#GW_ListView_Albums)
  Declare OpenAudio(PlayType = #Current)
  Declare Stop()
  Declare OpenCurrentAlbum()
  Declare SetAudioPosition()
  Declare AddCurrentTrackFromMyAudios()
  Declare.s GetTime(seconds)
  Declare UpdateMusicList()
  Declare OpenPostCurrentAudio()
  Declare AutoSwitchingTracks()
  Declare StartPlayed()
  Declare MarkAllPlayed()
  Declare LikePostCurrentAudio()
  Declare SkipAllTracksOfThisCurrentPost()
  ; Settings
  Declare OpenEditSubscribesWindow()
  Declare OpenSettingsWindow()
  
  ; В этой переменной хранится тип открытого плейлиста.
  Global PlaylistType
  
  ; Если равна 1, необходимо обновить информацию посредством GUI::SetPostInfo(),
  ; исходя из DB::TrackInfo.
  Global UpdatePostInfo
  
EndDeclareModule

Module Action
  
  ; Вспомогательная переменная для UpdateTrackBar().
  Global WaitingUpdateTrackBar

  ; Вспомогательная переменная для UpdateTrackBar().
  Global TempAudioDuration

  ; Здесь мы будем хранить информацию о воиспроизводимом сейчас треке. 
  Global CurrentTrack.DB::Audio

  ; Вернёт время в формате ММ:СС.
  Procedure.s GetTime(seconds)                                                                                       
    minute.s = Str(Minute(seconds)) 
    second.s = Right("0"+Str(Second(seconds)), 2)
    duration.s = minute.s+":"+second.s
    ProcedureReturn duration.s
  EndProcedure
  
  ; Используя DB::GetMusicList отобразит комозиции в главном окне.
  Procedure PasteMusicList(list_type=DB::#UnPlayed)
    lg_id = GUI::#GW_ListIcon_Playlist
    If IsGadget(lg_id)
      ClearGadgetItems(lg_id)
      PlaylistType = list_type
      DB::GetMusicList(DB::MusicList(), list_type)
      ForEach DB::MusicList()
        item.s = ""
        ;item.s +  Str(CountGadgetItems(lg_id)+1)
        item.s + Chr(10)
        item.s + DB::MusicList()\artist
        item.s + Chr(10)
        item.s + DB::MusicList()\title
        item.s + Chr(10)
        item.s + GetTime(DB::MusicList()\duration)
        AddGadgetItem(lg_id, num, item.s)
        SetGadgetItemData(lg_id, num, DB::MusicList()\fake_id)
        num + 1
      Next
    EndIf
  EndProcedure
  
  ; Используя VK::GetMusicList отобразит комозиции в главном окне.
  Procedure PasteVKMusicList(album_id = VK::#FakeAlbum_All)
    lg_id = GUI::#GW_ListIcon_Playlist
    If IsGadget(lg_id)
      ClearGadgetItems(lg_id)
      VK::GetMusicList(DB::MusicList(), album_id)
      ForEach DB::MusicList()
        item.s = ""
        ;item.s +  Str(CountGadgetItems(lg_id)+1)
        item.s + Chr(10)
        item.s + DB::MusicList()\artist
        item.s + Chr(10)
        item.s + DB::MusicList()\title
        item.s + Chr(10)
        item.s + GetTime(DB::MusicList()\duration)
        AddGadgetItem(lg_id, num, item.s)
        SetGadgetItemData(lg_id, num, DB::MusicList()\fake_id)
        num + 1
      Next
    EndIf
  EndProcedure
  
  ; Используя VK::GetAlbumsList() отобразит ВК альбомы в списке lw_id.
  Procedure PasteAlbumsList(lw_id = GUI::#GW_ListView_Albums)
    If IsGadget(lw_id)
      ClearGadgetItems(lw_id)
      VK::GetAlbumsList(VK::AlbumsList())
      
      AddGadgetItem(lw_id, 0, "Все аудиозаписи")
      SetGadgetItemData(lw_id, 0, VK::#FakeAlbum_All)
      
      num = 1
      ForEach VK::AlbumsList()
        AddGadgetItem(lw_id, -1, VK::AlbumsList()\title)
        SetGadgetItemData(lw_id, num, VK::AlbumsList()\id)
        num + 1
      Next
      
      For i=0 To CountGadgetItems(lw_id) - 1
        If GetGadgetItemData(lw_id, i) = Config::LastAlbum
          SetGadgetState(lw_id, i)
          Break
        EndIf
      Next
    EndIf
  EndProcedure
  
  ; Вспомогательная процедура для UpdateTrackBar().
  Procedure UpdateTrackBarForThread(null)
    Repeat
      If TempAudioDuration<>CurrentTrack\duration
        TempAudioDuration = CurrentTrack\duration
        SetGadgetAttribute(GUI::#GW_TrackBar_TrackScroll, #PB_TrackBar_Maximum, CurrentTrack\duration)
      EndIf  
          
      length = Player::GetLength()
      
      If length<>0
        position = Player::GetPosition()
        state = (position*CurrentTrack\duration)/length
        buf = Player::GetPercentageOfBufferFilled()
        SetGadgetState(GUI::#GW_TrackBar_TrackScroll, state)
        SetGadgetState(GUI::#GW_ProgressBar_TrackLoadProgress, buf)
      EndIf

      Delay(1000)
    ForEver
  EndProcedure  
    
  ; Будет обновлять положение ползунка на трекбаре в соответствии с прогрессом воспроизведения трека.
  Procedure StartUpdateTrackBar()
    If Not IsThread(WaitingUpdateTrackBar)
      WaitingUpdateTrackBar = CreateThread(@UpdateTrackBarForThread(), #Null)
    EndIf
  EndProcedure
  
  ; Прекратит обновление положения ползунка на трекбаре.
  Procedure StopUpdateTrackBar()
    KillThread(WaitingUpdateTrackBar)
  EndProcedure
  
  ; Используется для блокировки (если state = 1) кнопок, используемых для режима "списка непрослушанного".
  ; Для разблокировки нужно присвоить state значение 0.
  Procedure LockSpecialButtons(state)
    DisableGadget(GUI::#GW_Button_OpenPost, state)
    DisableGadget(GUI::#GW_Button_AddInMyAudios, state)
    DisableGadget(GUI::#GW_Button_SkipPost, state)
  EndProcedure
  
  ; Снимает выделение из списка воспроизведения.
  Procedure RemoveSelection()
    lg_id = GUI::#GW_ListIcon_Playlist
    For i=0 To CountGadgetItems(lg_id)-1
      If GetGadgetItemColor(lg_id, i, #PB_Gadget_BackColor) <> -1
        ; Я знаю про SetGadgetItemColor(lg_id, -1, #PB_Gadget_BackColor, -1, -1)
        ; Этот костыль был задуман не просто так, подробность здесь:
        ; http://purebasic.info/phpBB3ex/viewtopic.php?f=1&t=4480
        SetGadgetItemImage(lg_id, i, 0)
        SetGadgetItemColor(lg_id, i, #PB_Gadget_BackColor, -1, -1)
        Break
      EndIf
    Next
  EndProcedure
  
  ; Вспомогательная процедура для OpenAudio().
  Procedure OpenAudioForThread(PlayType)
    LockSpecialButtons(0)
    
    lg_id = GUI::#GW_ListIcon_Playlist
    
    ; Получаем информацию о том, какая композиция выбрана в плейлисте.
    If PlayType = #Current
      current_item = GetGadgetState(lg_id)
    ElseIf PlayType = #Previous
      ; Проверка на случай изменения плейлиста.
      If CountGadgetItems(lg_id) < current_item
        ; Плейлист изменился, начинаем сначала.
        current_item = 0
        SetGadgetState(lg_id, current_item)
      Else
        fake_id = GetGadgetItemData(lg_id, CurrentTrack\position)
        If fake_id <> CurrentTrack\fake_id
          ; Плейлист изменился, начинаем сначала.
          current_item = 0
          SetGadgetState(lg_id, current_item)
        Else
          current_item = CurrentTrack\position - 1
          SetGadgetState(lg_id, current_item)
        EndIf
      EndIf
    Else
      ; В режиме #Player_Feed следует при перелистывании треков отмечать пропущенную композицию прослушанной.
      If GUI::PlayerMode = GUI::#Player_Feed
        DB::Played(CurrentTrack\fake_id)
      EndIf
      ; Проверка на случай изменения плейлиста.
      If CountGadgetItems(lg_id)<current_item
        ; Плейлист изменился, начинаем сначала.
        current_item = 0
        SetGadgetState(lg_id, current_item)
      Else
        fake_id = GetGadgetItemData(lg_id, CurrentTrack\position)
        If fake_id <> CurrentTrack\fake_id
          ; Плейлист изменился, начинаем сначала.
          current_item = 0
          SetGadgetState(lg_id, current_item)
        Else
          current_item = CurrentTrack\position+1
          SetGadgetState(lg_id, current_item)
        EndIf
      EndIf
    EndIf
    
    If current_item > -1 And current_item < CountGadgetItems(lg_id)
      ; Отметим трек, который будет воспроизводится.
      RemoveSelection() ; Но для начала снимим отметку с предыдущего трека, если такая, конечно, была.
      SetGadgetItemImage(lg_id, current_item, ImageID(GUI::#Img_Current))
      SetGadgetItemColor(lg_id, current_item, #PB_Gadget_BackColor, 15854822, -1)
      fake_id = GetGadgetItemData(lg_id, current_item)
      
      ForEach DB::MusicList()
        If DB::MusicList()\fake_id = fake_id
          CurrentTrack\owner_id     = DB::MusicList()\owner_id
          CurrentTrack\audio_id     = DB::MusicList()\audio_id
          CurrentTrack\fake_id      = DB::MusicList()\fake_id
          CurrentTrack\duration     = DB::MusicList()\duration
          CurrentTrack\position     = current_item
          CurrentTrack\artist       = DB::MusicList()\artist
          CurrentTrack\title        = DB::MusicList()\title
          CurrentTrack\subscribe_id = DB::MusicList()\subscribe_id
          CurrentTrack\post_id      = DB::MusicList()\post_id
          CurrentTrack\info_id      = DB::MusicList()\info_id
          CurrentTrack\url          = DB::MusicList()\url
          
          If CurrentTrack\url = ""
            GUI::SetStatusBarText("Получаю URL текущего потока...")
            CurrentTrack\url = VK::GetAudioURL(CurrentTrack\audio_id, CurrentTrack\owner_id)
          EndIf  
            
          If CurrentTrack\url <> ""
            ; Ну и, собственно, воспроизводим трек.
            GUI::SetStatusBarText("Пытаюсь воспроизвести поток...")
      
            Player::Stop()
            Player::Open(CurrentTrack\url)
            
            GUI::SetStatusBarText("Воспроизведение.")
            SetGadgetState(GUI::#GW_ButtonImage_PlayAndPause, 1)
            
            SetWindowTitle(GUI::#Window_General, CurrentTrack\artist + " – " + CurrentTrack\title + " | Rangpur")
            StartUpdateTrackBar()
          Else
            SetGadgetState(GUI::#GW_ButtonImage_PlayAndPause, 0)
            GUI::SetStatusBarText("Стоп.")
          EndIf
          
          If GUI::PlayerMode = GUI::#Player_Feed
            old_info_id = DB::TrackInfo\id
            If DB::GetInfo(CurrentTrack\info_id, DB::TrackInfo)
              If old_info_id <> DB::TrackInfo\id
                ; Далее в дело вступает главный цикл из Rangpur.pb.
                Action::UpdatePostInfo = 1
              EndIf
            EndIf
          EndIf
        
          Break
        EndIf
      Next
      result = 1
    EndIf
    
    ProcedureReturn result
  EndProcedure
  
  ; При #Current начнёт воспроизведение выбранной композиции, при #Next следующей композиции.
  Procedure OpenAudio(PlayType = #Current)
    CreateThread(@OpenAudioForThread(), PlayType)
  EndProcedure
  
  ; Оставновит воспроизведение текущей композиции.
  Procedure Stop()
    Player::Stop()
    RemoveSelection()
    GUI::SetPostInfo("Rangpur!")
    SetGadgetState(GUI::#GW_ButtonImage_PlayAndPause, 0)
    SetGadgetState(GUI::#GW_TrackBar_TrackScroll, 0)
    SetGadgetState(GUI::#GW_ProgressBar_TrackLoadProgress, 0)
    LockSpecialButtons(1)
  EndProcedure
  
  Global WaitingSetAudioPosition
  
  Procedure WaitingSetAudioPositionForThread(null)
    StopUpdateTrackBar()
    Delay(1000)
    position = GetGadgetState(GUI::#GW_TrackBar_TrackScroll)+1
    length = Player::GetLength()
    state = (length*position)/CurrentTrack\duration
    Player::SetPosition(state)
    StartUpdateTrackBar()
  EndProcedure
  
  ; Изменяет позицию воспроизведения текущего трека.
  ; Срабатывает через секунду (ждёт изменение трекбара).
  Procedure SetAudioPosition()
    If Not IsThread(WaitingSetAudioPosition)
      WaitingSetAudioPosition = CreateThread(@WaitingSetAudioPositionForThread(), #Null)
    EndIf
  EndProcedure
  
  ; Вспомогательная процедура для AddCurrentTrackFromMyAudios().
  Procedure CloseSelectAlbumsWindow()
    If IsWindow(GUI::#Window_SelectAlbums)
      CloseWindow(GUI::#Window_SelectAlbums)
    EndIf
  EndProcedure
  
  ; Вспомогательная процедура для AddCurrentTrackFromMyAudios().
  Procedure SaveAudioInAlbum()
    If Config::IgnoreSAW <> 1
      lw_id = GUI::#SAW_ListView_AlbumsList
      album_id = GetGadgetItemData(lw_id, GetGadgetState(lw_id))
    Else
      album_id = VK::#FakeAlbum_All
    EndIf
      
    If album_id = VK::#FakeAlbum_All
      album_id = #PB_Ignore
    EndIf
    
    If Config::AutoLikePost.a = 1
      LikePostCurrentAudio()
    EndIf  
      
    VK::AddAudio(CurrentTrack\audio_id, CurrentTrack\owner_id, album_id)
    GUI::SetStatusBarText("Аудиозапись "+CurrentTrack\artist+" - "+CurrentTrack\title+" добавлена в ваши аудиозаписи.")
    CloseSelectAlbumsWindow()
  EndProcedure

  ; Добавит воспроизводимую в данный момент времени композицию в 'Мои аудиозаписи'.
  Procedure AddCurrentTrackFromMyAudios()
    If Config::IgnoreSAW = 1
      SaveAudioInAlbum()
    Else
      GUI::OpenWindow_SelectAlbums()
      PasteAlbumsList(GUI::#SAW_ListView_AlbumsList)
      SetGadgetState(GUI::#SAW_ListView_AlbumsList, 0)
      ; Если пользователь не создавал альбомов, то просто сохраняем в "Все аудиозаписи" и всё тут.
      If CountGadgetItems(GUI::#SAW_ListView_AlbumsList) = 1
        SaveAudioInAlbum()
        CloseSelectAlbumsWindow()
      Else
        SetGadgetText(GUI::#SAW_Text_Info, "Куда добавить "+CurrentTrack\artist+" - "+CurrentTrack\title+"?")
        BindGadgetEvent(GUI::#SAW_Button_Save, @SaveAudioInAlbum())
        BindGadgetEvent(GUI::#SAW_Button_Cancel, @CloseSelectAlbumsWindow())
      EndIf
    EndIf
  EndProcedure
  
  ; Отмечает все композиции как прослушанные.
  Procedure MarkAllPlayed()
    ;- А ведь можно пользователя повопрошать, действительно ли он хочет...
    ;- Будешь добавлять плейлисты - не забудь поправить этот код!
    result = MessageRequester("", "Вы действительно хотите отметить все композиции как прослушанные?", #PB_MessageRequester_YesNo)
    If result=#PB_MessageRequester_Yes
      If DB::MarkAllPlayed()
        ClearGadgetItems(GUI::#GW_ListIcon_Playlist)
        GUI::SetStatusBarText("Все композиции отмечены как прослушанные.")
      EndIf
    EndIf
  EndProcedure
  
  ; Хранит указатель потока, в котором выполняется проверка на новые аудиозаписи.
  ; Необходимо для UpdateMusicList().
  Global WaitingUpdateMusicList
  
  ; Вспомогательная процедура для UpdateMusicList().
  Procedure UpdateMusicListInThread(null)
    UpdateMusicList::Start()
    If GUI::PlayerMode = GUI::#Player_Feed
      Action::PasteMusicList()
    EndIf
  EndProcedure 
  
  ; Начнёт проверять новые аудиозаписи.
  Procedure UpdateMusicList()
    If Not IsThread(WaitingUpdateMusicList)
      SetMenuItemText(GUI::#GW_Menu_Others, GUI::#GW_Fun_Update, "Остановить сканирование аудиозаписей")
      WaitingUpdateMusicList = CreateThread(@UpdateMusicListInThread(), #Null)
    Else
      UpdateMusicList::StopUpdateList = 1
    EndIf
  EndProcedure
  
  ; Откроет link.s в браузере.
  Procedure OpenLink(link.s)
    If Config::WebBrowserPath <> "default"
      RunProgram(Config::WebBrowserPath, link.s, GetPathPart(Config::WebBrowserPath))
    Else
      If #PB_Compiler_OS = #PB_OS_Linux
        ; Спасибо lakomet! http://purebasic.info/phpBB3ex/viewtopic.php?f=17&t=1432&p=23001#p23001
        RunProgram("x-www-browser", link.s, GetHomeDirectory())
      Else
        RunProgram(link.s)
      EndIf
    EndIf
  EndProcedure
  
  ; Откроет в браузере пост, из которого была взята воспроизводимая сейчас композиция.
  Procedure OpenPostCurrentAudio()
    If Player::IsPlayed()<>0
      link.s="http://vk.com/wall"+Str(CurrentTrack\subscribe_id)+"_"+Str(CurrentTrack\post_id)
      OpenLink(link.s)
    EndIf
  EndProcedure
  
  ; Лайкнет пост в котором содержится проигрываемая композиция.
  Procedure LikePostCurrentAudio()
    ;- А если в этот момент ничего не воспроизводилось?
    VK::AddLikes(CurrentTrack\subscribe_id, CurrentTrack\post_id)
  EndProcedure
  
  ; Отметит как прослушанные все композиции поста, из которого был взят текущий трек.
  Procedure SkipAllTracksOfThisCurrentPost()
    If DB::PlayedPost(CurrentTrack\subscribe_id, CurrentTrack\post_id)
      ; Находим CurrentTrack\fake_id в списке MusicList().
      ForEach DB::MusicList()
        If search = 0 And DB::MusicList()\fake_id = CurrentTrack\fake_id
          search = 1
        EndIf
        ; И перебираем остающиеся элементы, пока не перестанут совпадать subscribe_id / post_id.
        If search = 1 And DB::MusicList()\post_id <> CurrentTrack\post_id
          fake_id = DB::MusicList()\fake_id
          Break
        EndIf
      Next
      ; И если элемент так и не найден - останавливаем воспроизведение.
      If fake_id = 0
        Stop()
      Else
        For num = 0 To CountGadgetItems(GUI::#GW_ListIcon_Playlist) - 1
          If GetGadgetItemData(GUI::#GW_ListIcon_Playlist, num) = fake_id
            SetGadgetState(GUI::#GW_ListIcon_Playlist, num)
            OpenAudio()
            Break
          EndIf
        Next
      EndIf
    EndIf
  EndProcedure
  
  ; Вспомогательная процедура для AutoSwitchingTracks().
  Procedure ThreadAutoSwitchingTracks(delay)
    Repeat
      If Player::IsPlayed()=0 ; Ожидаем завершение воспроизведения трека, и переходим к следующему треку.
        OpenAudio(#Next)
      EndIf
      Delay(delay)
    ForEver
  EndProcedure
  
  ; Отслеживает, завершилось ли воспроизведение композиции, если да - перелистывает трек.
  Procedure AutoSwitchingTracks()
    CreateThread(@ThreadAutoSwitchingTracks(), 1000)
  EndProcedure
  
  ; Начинает или возобновляет воспроизведение потока.
  Procedure StartPlayed()
    lg_id = GUI::#GW_ListIcon_Playlist
    If Not Player::Play()
      If GetGadgetState(lg_id)=-1
        SetGadgetState(lg_id, 0)
      EndIf
      OpenAudio()
    Else
      GUI::SetStatusBarText("Воспроизведение.")
    EndIf
  EndProcedure
  
  ; Вернёт id выбранного сейчас в #GW_ListView_Albums альбома, иначе 0.
  Procedure GetCurrentAlbumID()
    lw_id = GUI::#GW_ListView_Albums
    num = GetGadgetState(lw_id)
    ProcedureReturn GetGadgetItemData(lw_id, num)
  EndProcedure
  
  ; Получает id выбранного в #GW_ListView_Albums альбома и отображает его.
  Procedure OpenCurrentAlbum()
    album_id = GetCurrentAlbumID()
    Config::LastAlbum = album_id
    PasteVKMusicList(album_id)
  EndProcedure
  
  ; Если mode=#Player_Feed переключает плеер в режим прослушивания списка непрослушанных 
  ; композиций (собранных со стен групп), а также отобразит кнопки для взаимодействия с ними.
  ; А mode=#Player_VKMusic заставит плеер перейти в режим прослушивания вконтактовских альбомов,
  ; и вместо специфичных для #Player_Feed кнопок отобразит список альбомов.
  Procedure SwitchPlayerMode(player_mode)
    GUI::SwitchPlayerMode(player_mode)
    If player_mode = GUI::#Player_Feed
      If Not IsThread(WaitingUpdateMusicList)
        Action::PasteMusicList()
      EndIf
      Stop()
    Else
      Action::PasteAlbumsList()
      Action::PasteVKMusicList(Config::LastAlbum)
      Stop()
    EndIf
  EndProcedure  
  
  ;- SETTINGS
  ;{
  
  ; Закрывает окно выбора подписок.
  Procedure CloseEditSubscribesWindow()
    If IsWindow(GUI::#Window_EditSubscribes)
      CloseWindow(GUI::#Window_EditSubscribes)
    EndIf
  EndProcedure
  
  ; Запросит у ВК и отобразит в окне #Window_EditSubscribes подписки.
  Procedure PasteSubscribesList()
    lg_id = GUI::#ESW_ListIcon_Subscribes
    If IsGadget(lg_id)
      ClearGadgetItems(lg_id)
      If VK::GetSubscribesList(VK::SubscribesList.VK::Subscribe())
        ForEach VK::SubscribesList()
          item.s = ""
          item.s + Chr(10)
          item.s + VK::SubscribesList()\name
          item.s + Chr(10)
          item.s + VK::SubscribesList()\subscribe_id
          AddGadgetItem(lg_id, num, item.s)
          SetGadgetItemData(lg_id, num, VK::SubscribesList()\subscribe_id)
          ;- Не желаете делать это одним запросом к БД и прогоном циклом в памяти?
          If DB::IsSubscribe(VK::SubscribesList()\subscribe_id)
            SetGadgetItemState(lg_id, num, #PB_ListIcon_Checked)
          EndIf
          num + 1
        Next
      EndIf
    EndIf
  EndProcedure  
  
  ; Сохранит выбранные подписки в БД.
  Procedure SaveEditSubscribesList()
    ;- COMMIT юзать будем?
    lg_id = GUI::#ESW_ListIcon_Subscribes
    NewList TempList.s()
    
    For item = 0 To CountGadgetItems(lg_id)-1
      If GetGadgetItemState(lg_id, item) & #PB_ListIcon_Checked
        sub_id = GetGadgetItemData(lg_id, item)
        name.s = GetGadgetItemText(lg_id, item, 1)
        If DB::IsSubscribe(sub_id)=0
          AddElement(TempList.s())
          TempList() = DB::ReturnAddSubscribeQueryString(sub_id, name)
        EndIf
      Else
        sub_id = GetGadgetItemData(lg_id, item)
        If DB::IsSubscribe(sub_id)=1
          AddElement(TempList.s())
          TempList() = DB::ReturnRemoveSubscribeQueryString(sub_id)
        EndIf
      EndIf
    Next
    
    If ListSize(TempList.s()) > 0 
      DB::QueriesInTheTransaction(TempList.s())
    EndIf 
    
    CloseEditSubscribesWindow()
    If IsWindow(GUI::#Window_Settings)
      SetGadgetText(GUI::#SW_Text_CountSubscribes, Str(DB::GetSubscribesCount()))
    Else
      UpdateMusicList()
    EndIf
  EndProcedure
  
  ; Открывает окно выбора подписок.
  Procedure OpenEditSubscribesWindow()
    Config::FirstAuth = 0
    GUI::OpenWindow_EditSubscribes()
    PasteSubscribesList()
    BindGadgetEvent(GUI::#ESW_Button_Save, @SaveEditSubscribesList())
    BindGadgetEvent(GUI::#ESW_Button_Cancel, @CloseEditSubscribesWindow())
  EndProcedure
  
  Procedure SaveOthersSetting()
    If GetGadgetState(GUI::#SW_Option_WebBrowser_Default) = 1
      Config::WebBrowserPath.s = "default"
    Else
      Config::WebBrowserPath.s = GetGadgetText(GUI::#SW_String_Path_WebBrowser)
    EndIf
    
    If GetGadgetState(GUI::#SW_Option_Artwork_Default) = 1
      GUI::DefaultArtwork.a = 1
    Else
      GUI::DefaultArtwork.a = 0
      GUI::ArtworkPath.s = GetGadgetText(GUI::#SW_String_Path_Artwork)
    EndIf
    
    If GetGadgetState(GUI::#SW_Checkbox_DisableAlbumsWindow) = 1
      Config::IgnoreSAW.a = 1
    Else 
      Config::IgnoreSAW.a = 0
    EndIf
    
    If GetGadgetState(GUI::#SW_Checkbox_AutoLikePost) = 1
      Config::AutoLikePost.a = 1
    Else 
      Config::AutoLikePost.a = 0
    EndIf
  EndProcedure
  
  Procedure Purging()
    result = MessageRequester("", "Вы действительно хотите очистить БД от прослушанных композиций?", #PB_MessageRequester_YesNo)
    If result = #PB_MessageRequester_Yes
      DB::Purging()
    EndIf
  EndProcedure
  
  Procedure SelectBrowserOptionManual()
    DisableGadget(GUI::#SW_String_Path_WebBrowser, 0)
    DisableGadget(GUI::#SW_Button_Path_WebBrowser, 0)
  EndProcedure
  
  Procedure SelectBrowserOptionDefault()
    DisableGadget(GUI::#SW_String_Path_WebBrowser, 1)
    DisableGadget(GUI::#SW_Button_Path_WebBrowser, 1)
  EndProcedure
  
  Procedure SelectArtworkOptionManual()
    DisableGadget(GUI::#SW_String_Path_Artwork, 0)
    DisableGadget(GUI::#SW_Button_Path_Artwork, 0)
  EndProcedure
  
  Procedure SelectArtworkOptionDefault()
    DisableGadget(GUI::#SW_String_Path_Artwork, 1)
    DisableGadget(GUI::#SW_Button_Path_Artwork, 1)
  EndProcedure
  
  Procedure SearchArtwork()
    pattern.s = "JPEG (*.jpg)|*.jpg;*.jpeg|PNG (*.png)|*.png"
    path.s = OpenFileRequester("Выберите изображение:", GetCurrentDirectory(), pattern.s, 0)
    SetGadgetText(GUI::#SW_String_Path_Artwork, path.s)
  EndProcedure
  
  Procedure SearchWebBrowser()
    pattern.s = "Executable (*.exe)|*.exe"
    path.s = OpenFileRequester("Выберите исполняемый файл браузера:", GetCurrentDirectory(), pattern.s, 0)
    SetGadgetText(GUI::#SW_String_Path_WebBrowser, path.s)
  EndProcedure
  
  ; Очистка БД.
  Procedure ClearDB()
    result = MessageRequester("", "Вы действительно хотите очистить БД?"+Chr(10)+"Список ваших подписок будет сохранён.", #PB_MessageRequester_YesNo)
    If result = #PB_MessageRequester_Yes
      DB::Clear()
    EndIf
  EndProcedure
  
  ; Пересоздаёт БД и позволяет авторизироваться под другим пользователем.
  Procedure ReCreateAndReInit()
    result = MessageRequester("", "Вы действительно хотите пересоздать БД и удалить данные авторизации?", #PB_MessageRequester_YesNo)
    If result = #PB_MessageRequester_Yes
      If DB::RemoveDB()
        DB::Init(Config::DatabasePath)
        VK::CurrentSecret.s  = ""
        VK::CurrentUser.l    = 0
        VK::CurrentToken.s   = ""
        VK::FirstName.s      = ""
        VK::LastName.s       = ""
        VK::UserPhotoURL.s   = ""
        VK::Init()
      EndIf
    EndIf
  EndProcedure
  
  ; Открывает окно с настройками.
  Procedure OpenSettingsWindow()                                                                                       
    GUI::OpenWindow_Settings()

    SetGadgetText(GUI::#SW_Text_UserName, VK::FirstName.s+Chr(10)+VK::LastName.s)
    GUI::SetImageInWebGadget(GUI::#SW_WebView_UserAvatar, VK::UserPhotoURL.s)

    SetGadgetText(GUI::#SW_Text_CountSubscribes, Str(DB::GetSubscribesCount()))
    SetGadgetText(GUI::#SW_String_Path_WebBrowser, Config::WebBrowserPath.s)
    SetGadgetText(GUI::#SW_String_Path_Artwork, GUI::ArtworkPath.s)
    
    If GetGadgetState(GUI::#SW_Option_WebBrowser_Default) = 1
      SetGadgetState(GUI::#SW_Option_WebBrowser_Default, 1)
      DisableGadget(GUI::#SW_String_Path_WebBrowser, 1)
      DisableGadget(GUI::#SW_Button_Path_WebBrowser, 1)
    Else
      SetGadgetState(GUI::#SW_Option_WebBrowser_Manual, 1)
      DisableGadget(GUI::#SW_String_Path_WebBrowser, 0)
      DisableGadget(GUI::#SW_Button_Path_WebBrowser, 0)
    EndIf
    
    If GUI::DefaultArtwork.a = 1
      DisableGadget(GUI::#SW_String_Path_Artwork, 1)
      DisableGadget(GUI::#SW_Button_Path_Artwork, 1)
      SetGadgetState(GUI::#SW_Option_Artwork_Default, 1)
    Else
      SetGadgetState(GUI::#SW_Option_Artwork_Manual, 1)
      DisableGadget(GUI::#SW_String_Path_Artwork, 0)
      DisableGadget(GUI::#SW_Button_Path_Artwork, 0)
    EndIf

    If Config::IgnoreSAW.a = 1
      SetGadgetState(GUI::#SW_Checkbox_DisableAlbumsWindow, 1)
    EndIf
    
    If Config::AutoLikePost.a = 1
      SetGadgetState(GUI::#SW_Checkbox_AutoLikePost, 1)
    EndIf

    BindGadgetEvent(GUI::#SW_Button_EditSubsList, @OpenEditSubscribesWindow()) 
    BindGadgetEvent(GUI::#SW_Button_SaveOthers, @SaveOthersSetting()) 
    BindGadgetEvent(GUI::#SW_Option_WebBrowser_Manual, @SelectBrowserOptionManual())
    BindGadgetEvent(GUI::#SW_Option_WebBrowser_Default, @SelectBrowserOptionDefault())
    BindGadgetEvent(GUI::#SW_Option_Artwork_Manual, @SelectArtworkOptionManual())
    BindGadgetEvent(GUI::#SW_Option_Artwork_Default, @SelectArtworkOptionDefault())
    BindGadgetEvent(GUI::#SW_Button_ClearDB, @ClearDB())
    BindGadgetEvent(GUI::#SW_Button_ReCreatedDB, @ReCreateAndReInit())
    BindGadgetEvent(GUI::#SW_Button_Path_Artwork, @SearchArtwork())
    BindGadgetEvent(GUI::#SW_Button_Path_WebBrowser, @SearchWebBrowser())
    
  EndProcedure
  
  ;}
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 50
; FirstLine = 45
; Folding = 80-------
; EnableUnicode
; EnableXP