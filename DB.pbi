﻿
DeclareModule DB
  
  Enumeration ResultType
    #UnPlayed
    #All
  EndEnumeration
  
  Structure Audio
    fake_id.l
    audio_id.l
    owner_id.l
    artist.s
    title.s
    duration.l
    genre_id.a
    subscribe_id.l
    post_id.l
    info_id.l
    played.a
    position.l
    url.s
  EndStructure
  
  Structure Subscribe
    subscribe_id.l
    last_date.l
    name.s
  EndStructure
  
  Structure Info
    id.l
    text.s
    photo_75.s
    photo_130.s
    photo_604.s
  EndStructure
  
  NewList MusicList.Audio()
  NewList SubscribesList.Subscribe()
  TrackInfo.Info

  Declare Init(database_path.s)
  Declare Free()
  Declare Clear()
  Declare RemoveDB()
  Declare GetMusicList(List MusicList.Audio(), list_type=#UnPlayed)
  Declare GetSubscribesList(List SubscribesList.Subscribe())
  Declare GetSubscribesCount()
  Declare GetLastDateFromSubscribe(subscribe_id)
  Declare SetLastDateFromSubscribe(sub_id, last_date)
  Declare AddInfo(subscribe_id, post_id, text.s, *Photos.DB::Info, repost_subscribe_id = 0)
  Declare GetInfo(info_id, *TrackInfo.Info)
  Declare IsSubscribe(subscribe_id)
  Declare Played(fake_audio_id)
  Declare NumberOfAudios()
  Declare QueriesInTheTransaction( List Requests.s() )
  Declare Purging()
  Declare.s ReturnAddAudioQueryString(audio_id, owner_id, artist.s, title.s, duration, genre_id, subscribe_id, post_id, info_id)
  Declare.s ReturnAddSubscribeQueryString(subscribe_id, name.s)
  Declare.s ReturnRemoveSubscribeQueryString(subscribe_id)
  Declare MarkAllPlayed()
  Declare PlayedPost(subscribe_id, post_id)
  
EndDeclareModule

Module DB

  ; Здесь будем хранить хэндл БД.
  Global DatabaseID
  
  ; А здесь её путь.
  Global DatabasePath.s
  
  ; А здесь мьютекс для процедур обращающихся к БД, чтоб приложка не падала при одновременных обращениях к БД.
  Global MutexDB = CreateMutex()

  ; Функции для инициализирования БД.
  ;{
  
  Procedure DB_CreateMusicList(id_db)
    tablestring.s + "CREATE TABLE musiclist ("
    tablestring.s + "fake_id       INTEGER PRIMARY KEY,"
    tablestring.s + "audio_id      INTEGER NOT NULL,"
    tablestring.s + "owner_id      INTEGER NOT NULL,"
    tablestring.s + "artist        TEXT NOT NULL,"
    tablestring.s + "title         TEXT NOT NULL,"
    tablestring.s + "duration      INTEGER NOT NULL,"
    tablestring.s + "genre_id      INTEGER NOT NULL,"
    tablestring.s + "subscribe_id  INTEGER NOT NULL,"
    tablestring.s + "post_id       INTEGER NOT NULL,"
    tablestring.s + "info_id       INTEGER NOT NULL,"
    tablestring.s + "played        INTEGER NOT NULL DEFAULT 0);"
    If DatabaseUpdate(id_db, tablestring.s) = 0 
      Debug "DB_CreateMusicList > critical error > "+DatabaseError()
      MessageRequester("DB_CreateMusicList()", DatabaseError()+Chr(10)+"Приложение будет закрыто.")
      End
    EndIf
    FinishDatabaseQuery(id_db)
  EndProcedure
  
  Procedure DB_CreateSubscribesList(id_db)
    tablestring.s + "CREATE TABLE subscribeslist ("
    tablestring.s + "subscribe_id   INTEGER PRIMARY KEY,"
    tablestring.s + "name           TEXT NOT NULL,"
    tablestring.s + "last_date      INTEGER NOT NULL DEFAULT 0);"
    If DatabaseUpdate(id_db, tablestring.s) = 0 
      Debug "DB_Createsubscribeslist > critical error > "+DatabaseError()
      MessageRequester("DB_Createsubscribeslist()", DatabaseError()+Chr(10)+"Приложение будет закрыто.")
      End
    EndIf
    FinishDatabaseQuery(id_db)
  EndProcedure
  
  Procedure DB_CreateInformationsList(id_db)
    tablestring.s + "CREATE TABLE informationslist ("
    tablestring.s + "info_id              INTEGER PRIMARY KEY,"
    tablestring.s + "text                 TEXT NOT NULL,"
    tablestring.s + "photo_75             TEXT NOT NULL,"
    tablestring.s + "photo_130            TEXT NOT NULL,"
    tablestring.s + "photo_604            TEXT NOT NULL,"
    tablestring.s + "subscribe_id         INTEGER NOT NULL,"
    tablestring.s + "post_id              INTEGER NOT NULL,"
    tablestring.s + "repost_subscribe_id  INTEGER NOT NULL);"
    
    If DatabaseUpdate(id_db, tablestring.s) = 0 
      Debug "DB_CreateInformationsList > critical error > "+DatabaseError()
      MessageRequester("DB_CreateInformationsList()", DatabaseError()+Chr(10)+"Приложение будет закрыто.")
      End
    EndIf
    FinishDatabaseQuery(id_db)
  EndProcedure
  
  Procedure DB_OpenDatabase(db_name.s)
    DatabaseID = OpenDatabase(#PB_Any, db_name.s, "", "", #PB_Database_SQLite)
    ProcedureReturn DatabaseID
  EndProcedure
  
  Procedure DB_IndexInit(db_name.s)
    UseSQLiteDatabase()
    DatabasePath.s = db_name.s
    ; Проверяем, существует ли БД, если нет - создаём её.
    If ReadFile(0, db_name.s)
      CloseFile(0)
      DatabaseID=DB_OpenDatabase(db_name.s)
    Else
      If DB_OpenDatabase(db_name.s)
        Debug "DB_IndexInit() > critical error > unknow error"
        MessageRequester("DB_IndexInit()", "Невозможно открыть базу данных."+Chr(10)+"Вероятно, база данных занята другим приложением."+Chr(10)+"Приложение будет закрыто.")
        End
      Else
        If CreateFile(0, db_name.s)
          CloseFile(0)
          DB_OpenDatabase(db_name.s)
          id_db=DatabaseID
          If id_db>0
            DB_CreateMusicList(id_db)
            DB_CreateSubscribesList(id_db)
            DB_CreateInformationsList(id_db)
          EndIf
        Else
          Debug "DB_IndexInit() > critical error > "+DatabaseError()
          MessageRequester("DB_IndexInit()", "Невозможно создать базу данных."+Chr(10)+"Приложение будет закрыто.")
          End
        EndIf
      EndIf
    EndIf
  EndProcedure
  
  ;} 
  
  ; Открывает БД (или создаёт, в случае её отсутствия) делая доступным использование функций из данного модуля.
  Procedure Init(database_path.s)
    ; Проверяем, не была ли инициализирована БД ранее.
    If DatabaseID = 0
      ; Проверяем, существует ли БД, если нет - создаём её.
      DB_IndexInit(database_path.s)
      ProcedureReturn 1
    Else
      ProcedureReturn 0
    EndIf
  EndProcedure
  
  ; Закрывает текущую БД.
  Procedure Free()
    If DatabaseID <> 0
      CloseDatabase(DatabaseID)
      DatabaseID = 0
      result = 1
    Else
      result = 0
    EndIf
    ProcedureReturn result
  EndProcedure

  ; Выполнит запросы из Requests.s() к БД.
  ; Вернёт 1, в случае успеха.
  ;- Почему-то всегда возращает "not an error".
  ; Для быстрого сохранения аудиозаписей.
  Procedure QueriesInTheTransaction( List Requests.s() )
    
    ForEach Requests.s()
      requests.s + Requests.s()+Chr(10)
    Next

    ;query.s = "PRAGMA auto_vacuum = 1;"+Chr(10)
    ;query.s = "PRAGMA synchronous = NORMAL;"+Chr(10)
    query.s + "BEGIN TRANSACTION;"+Chr(10)
    query.s + requests.s
    query.s + "COMMIT;"+Chr(10)
    ;Debug query.s
    
    LockMutex(MutexDB)
    
    If DatabaseUpdate(DatabaseID, query.s)
      FinishDatabaseQuery(DatabaseID)
      result = 1
    Else
      Debug "QueriesInTheTransaction() > error > "+DatabaseError()
    EndIf
    
    UnlockMutex(MutexDB)
    
    ClearList(Requests.s())
    ProcedureReturn result
  
  EndProcedure
  
  ; Загружает из БД в MusicList() определённый (переменной list_type) список композций.
  ; #UnPlayed - непрослушанные.
  ; #All - все композции.
  Procedure GetMusicList(List MusicList.Audio(), list_type=#UnPlayed)
    ClearList(MusicList())
    
    columns.s = "fake_id, "
    columns.s + "audio_id, "
    columns.s + "owner_id, "
    columns.s + "artist, "
    columns.s + "title, "
    columns.s + "duration, "
    columns.s + "genre_id, "
    columns.s + "subscribe_id, "
    columns.s + "post_id, "
    columns.s + "info_id, "
    columns.s + "played "
    
    If list_type=#UnPlayed
      query.s="SELECT "+columns.s+" FROM musiclist WHERE played='0';"
    Else
      query.s="SELECT "+columns.s+" FROM musiclist;"
    EndIf
    
    LockMutex(MutexDB)
    
    If DatabaseQuery(DatabaseID, query.s)
      While NextDatabaseRow(DatabaseID)
        AddElement(MusicList())
        MusicList()\fake_id=GetDatabaseLong(DatabaseID, 0)
        MusicList()\audio_id=GetDatabaseLong(DatabaseID, 1)
        MusicList()\owner_id=GetDatabaseLong(DatabaseID, 2)
        MusicList()\artist=GetDatabaseString(DatabaseID, 3)
        MusicList()\title=GetDatabaseString(DatabaseID, 4)
        MusicList()\duration=GetDatabaseLong(DatabaseID, 5)
        MusicList()\genre_id=GetDatabaseLong(DatabaseID, 6)
        MusicList()\subscribe_id=GetDatabaseLong(DatabaseID, 7)
        MusicList()\post_id=GetDatabaseLong(DatabaseID, 8)
        MusicList()\info_id=GetDatabaseLong(DatabaseID, 9)
        MusicList()\played=GetDatabaseLong(DatabaseID, 10)
      Wend
      FinishDatabaseQuery(DatabaseID)
      result=ListSize( MusicList() )
    Else
      Debug "GetMusicList() > error > "+DatabaseError()
      result=0
    EndIf
    
    UnlockMutex(MutexDB)
    
    ProcedureReturn result
  EndProcedure

  ; Загружает из БД в SubscribesList() все подписки.
  Procedure GetSubscribesList(List SubscribesList.Subscribe())
    ClearList(SubscribesList())

    query.s="SELECT subscribe_id, last_date, name FROM subscribeslist;"
    
    LockMutex(MutexDB)
    
    If DatabaseQuery(DatabaseID, query.s)
      While NextDatabaseRow(DatabaseID)
        AddElement(SubscribesList())
        SubscribesList()\subscribe_id=GetDatabaseLong(DatabaseID, 0)
        SubscribesList()\last_date=GetDatabaseLong(DatabaseID, 1)
        SubscribesList()\name=GetDatabaseString(DatabaseID, 2)
      Wend
      FinishDatabaseQuery(DatabaseID)
      SortStructuredList(SubscribesList(), #PB_Sort_Descending, OffsetOf(Subscribe\last_date), TypeOf(Subscribe\last_date))
      result=ListSize( SubscribesList() )
    Else
      Debug "GetSubscribesList() > error > "+DatabaseError()
      result=0
    EndIf
    
    UnlockMutex(MutexDB)
    
    ProcedureReturn result
  EndProcedure
  
  ; Вернёт колличество подписок, за которыми следит Rangpur.
  Procedure GetSubscribesCount()
    query.s="SELECT COUNT(*) FROM subscribeslist;"
    
    LockMutex(MutexDB)
    
    If DatabaseQuery(DatabaseID, query.s)
      If NextDatabaseRow(DatabaseID)
        result = GetDatabaseLong(DatabaseID, 0)
      EndIf
      FinishDatabaseQuery(DatabaseID)
    Else
      Debug "GetSubscribesCount() > error > "+DatabaseError()
      result = -1
    EndIf
    
    UnlockMutex(MutexDB)
    
    ProcedureReturn result
  EndProcedure
  
  ; Вернёт дату последнего посканированного поста в подписке.
  Procedure GetLastDateFromSubscribe(subscribe_id)
    query.s = "SELECT last_date FROM subscribeslist WHERE subscribe_id='"+Str(subscribe_id)+"';"
    
    LockMutex(MutexDB)
    
    If DatabaseQuery(DatabaseID, query.s)
      If NextDatabaseRow(DatabaseID)
        result = GetDatabaseLong(DatabaseID, 0)
      EndIf
      FinishDatabaseQuery(DatabaseID)
    Else
      Debug "SetLastDateFromSubscribe() > error > "+DatabaseError()
    EndIf
    
    UnlockMutex(MutexDB)
    
    ProcedureReturn result
  EndProcedure
  
  ; Вернёт 1, если подписка есть в БД, иначе 0.
  Procedure IsSubscribe(subscribe_id)
    LockMutex(MutexDB)
    If DatabaseQuery(DatabaseID, "SELECT * FROM subscribeslist WHERE subscribe_id="+Str(subscribe_id)+";")
      If NextDatabaseRow(DatabaseID)
        result = 1
      EndIf
      FinishDatabaseQuery(DatabaseID)
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure  
    
  ; Изменит дату последнего посканированного поста в подписке.
  ; Вернёт 1 в случае успеха.
  Procedure SetLastDateFromSubscribe(sub_id, last_date)
    query.s="UPDATE subscribeslist SET last_date='"+Str( last_date )+"' WHERE subscribe_id='"+Str( sub_id )+"';"
    LockMutex(MutexDB)
    If DatabaseUpdate(DatabaseID, query.s)
      FinishDatabaseQuery(DatabaseID)
      result=1
    Else
      Debug "SetLastDateFromSubscribe() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
  ;- Есть дублирующийся код!
  
  ; ВНЕЗАПНО, создаёт строку в таблице БД.
  Procedure AddStringInDB(table_name.s, List DataList.s())
    ;- А нужно ли?
    ResetList(DataList())
    ; [шутка про экранирование символов].
    If ListSize(DataList())
      While NextElement(DataList())
        element.s=DataList()
        ; Вот так, легко и непринуждённо, можно избавится от проблем при добавлении строки, содержащей в себе ' .
        If FindString(element.s, "'")
          element.s=ReplaceString(element.s, "'", "''")
        EndIf
        element.s="'"+element.s+"',"
        datastring.s + element.s
      Wend
      FreeList(DataList())
      ; Это самое лучшее, что смог родить мой мозг в 5 утра.
      ; Весьма логичный способ избавится от запятой, неправда ли? :Р
      datastring.s=Left(datastring.s, Len(datastring.s)-1)
      tablestring.s="INSERT INTO "+table_name.s+" VALUES ("+datastring.s+");"
      LockMutex(MutexDB)
      If DatabaseUpdate(DatabaseID, tablestring.s)
        FinishDatabaseQuery(DatabaseID)
        result = 1
      Else
        Debug "AddStringInDB() > error > "+DatabaseError()
      EndIf
      UnlockMutex(MutexDB)
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Сохраняет вспомогательную информацию в БД.
  ; Вернёт info_id.
  Procedure AddInfo(subscribe_id, post_id, text.s, *Photos.DB::Info, repost_subscribe_id = 0)
    LockMutex(MutexDB)
    ;- А нельзя ли это всё объеденить в один запрос?
    If DatabaseQuery(DatabaseID, "SELECT MAX(info_id) FROM informationslist;")
      NextDatabaseRow(DatabaseID)
      maxid = GetDatabaseLong(DatabaseID, 0) + 1
      
      If FindString(text.s, "'")
        text.s = ReplaceString(text.s, "'", "''")
      EndIf

      string.s = "INSERT INTO informationslist "+
                  "(info_id, text, photo_75, photo_130, photo_604, "+
                  "subscribe_id, post_id, repost_subscribe_id) "+
                  "VALUES ("+Str(maxid)+",'"+text.s+"', '"+*Photos\photo_75+"', '"+
                  *Photos\photo_130+"', '"+*Photos\photo_604+"', "+Str(subscribe_id)+","+
                  Str(post_id)+", "+Str(repost_subscribe_id)+");"

      If DatabaseUpdate(DatabaseID, string.s)
        FinishDatabaseQuery(DatabaseID)
        result = maxid
      Else
        Debug "AddInfo() > error > "+DatabaseError()
        result = 0
      EndIf
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
  ; Вернёт в переменной TrackInfo вспомогательную информацию info_id.
  Procedure GetInfo(info_id, *TrackInfo.Info)
    query.s = "SELECT text, photo_75, photo_130, photo_604 FROM informationslist WHERE info_id="+Str(info_id)+";"

    LockMutex(MutexDB)
    
    If DatabaseQuery(DatabaseID, query.s)
      If NextDatabaseRow(DatabaseID)
        *TrackInfo\text = GetDatabaseString(DatabaseID, 0)
        *TrackInfo\photo_75 = GetDatabaseString(DatabaseID, 1)
        *TrackInfo\photo_130 = GetDatabaseString(DatabaseID, 2)
        *TrackInfo\photo_604 = GetDatabaseString(DatabaseID, 3)
        *TrackInfo\id = info_id
      EndIf
      FinishDatabaseQuery(DatabaseID)
      result = 1
    Else
      Debug "GetInfo() > error > "+DatabaseError()
      result=0
    EndIf
    
    UnlockMutex(MutexDB)
    
    ProcedureReturn result
  EndProcedure
  
  ; Превратит описание аудиозаписи в SQL запрос.
  Procedure.s ReturnAddAudioQueryString(audio_id, owner_id, artist.s, title.s, duration, 
                                        genre_id, subscribe_id, post_id, info_id)
    If FindString(artist.s, "'")
      artist.s = ReplaceString(artist.s, "'", "''")
    EndIf
    
    If FindString(title.s, "'")
      title.s = ReplaceString(title.s, "'", "''")
    EndIf

    result.s = "INSERT INTO musiclist "
    result.s + "(audio_id, owner_id, artist, title, duration, "
    result.s + "genre_id, subscribe_id, post_id, info_id) "
    result.s + "VALUES ("+Str(audio_id)+", "+Str(owner_id)+", '"+artist.s+"', "
    result.s +            "'"+title.s+"', "+Str(duration)+", "+Str(genre_id)+", "
    result.s +            Str(subscribe_id)+", "+Str(post_id)+", "+Str(info_id)+" );"
    
    ProcedureReturn result.s
  EndProcedure
  
  ; Вернёт колличество аудиозапией в БД.
  Procedure NumberOfAudios()
    LockMutex(MutexDB)
    If DatabaseQuery(DatabaseID, "SELECT MAX(fake_id) FROM musiclist;")
      NextDatabaseRow(DatabaseID)
      fake_id=GetDatabaseLong(DatabaseID, 0)
      FinishDatabaseQuery(DatabaseID)
    Else
      Debug "NumberOfAudios() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn fake_id
  EndProcedure

  ; Отметит аудиозапись как воспроизведённую.
  ; Вернёт 1 в случае успеха, иначе 0.
  Procedure Played(fake_audio_id)
    LockMutex(MutexDB)
    If DatabaseUpdate(DatabaseID, "UPDATE musiclist SET played = 1 WHERE fake_id='"+Str( fake_audio_id )+"';")
      FinishDatabaseQuery(DatabaseID)
      result=1
    Else
      Debug "Played() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
  ; Отметит как прослушанные все композиции поста post_id группы subscribe_id.
  ; Вернёт 1 в случае успеха, иначе 0.
  Procedure PlayedPost(subscribe_id, post_id)
    LockMutex(MutexDB)
    If DatabaseUpdate(DatabaseID, "UPDATE musiclist SET played = 1 WHERE post_id='"+Str( post_id )+"' AND subscribe_id='"+Str( subscribe_id )+"';")
      FinishDatabaseQuery(DatabaseID)
      result=1
    Else
      Debug "PlayedPost() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure

  ; Отмечает все композиции как прослушанные.
  ; Вернёт 1 в случае успеха, иначе 0.
  Procedure MarkAllPlayed()
    LockMutex(MutexDB)
    If DatabaseUpdate(DatabaseID, "UPDATE musiclist SET played = 1;")
      FinishDatabaseQuery(DatabaseID)
      result=1
    Else
      Debug "MarkAllPlayed() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
  ; Превратит описание группы в SQL запрос.
  Procedure.s ReturnAddSubscribeQueryString(subscribe_id, name.s)
    ; Формируем дату, чтоб потом при первом парсинге подписки получить посты за неделю.
    date = Date()
    last_date = AddDate(date, #PB_Date_Week, -1)
    
    If FindString(name.s, "'")
      name.s = ReplaceString(name.s, "'", "''")
    EndIf

    result.s = "INSERT INTO subscribeslist (subscribe_id, name, last_date) "
    result.s + "VALUES ("+Str(subscribe_id)+", '"+name.s+"', "+Str(last_date)+");"
    
    ProcedureReturn result.s
  EndProcedure
  
  ; Создаст SQL запрос удаления группы.
  Procedure.s ReturnRemoveSubscribeQueryString(subscribe_id)
    ProcedureReturn "DELETE FROM subscribeslist WHERE subscribe_id="+Str(subscribe_id)+";"
  EndProcedure

  ; Удаляет воспроизведённые треки из БД.
  ; Вернёт 1 в случае успеха, иначе 0.
  Procedure Purging()
    LockMutex(MutexDB)
    If DatabaseUpdate(DatabaseID, "DELETE FROM musiclist WHERE played='1';")
      result=1
      FinishDatabaseQuery(DatabaseID)
    Else
      Debug "Purging() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
  ; Очищает таблицы musiclist и informationslist в БД.
  ; Вернёт 1 в случае успеха, иначе 0.
  Procedure Clear()
    LockMutex(MutexDB)
    If DatabaseUpdate(DatabaseID, "DELETE FROM musiclist;")
      If DatabaseUpdate(DatabaseID, "DELETE FROM informationslist;")
        result = 1
      EndIf
      FinishDatabaseQuery(DatabaseID)
    Else
      Debug "Clear() > error > "+DatabaseError()
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
  ; Закрывает и удаляет текущую БД.
  Procedure RemoveDB()
    LockMutex(MutexDB)
    If Free()
      If DeleteFile(DatabasePath.s)
        result = 1
      Else
        Debug "RemoveDB() > error > не удалось удалить БД "+DatabasePath.s
      EndIf
    Else
      Debug "RemoveDB() > error > не могу закрыть БД "+DatabasePath.s
    EndIf
    UnlockMutex(MutexDB)
    ProcedureReturn result
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 53
; FirstLine = 36
; Folding = 4-----
; EnableUnicode
; EnableXP