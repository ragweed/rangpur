﻿
DeclareModule Network
  Declare.s GetURL(url.s)
EndDeclareModule

Module Network
  
  InitNetwork()
  
  ; А здесь мьютекс для процедур обращающихся к сети, чтоб приложка не падала...
  Global MutexNetwork = CreateMutex()

  ; Позаимствовал из HTTP Library (http://www.purebasic.fr/english/viewtopic.php?f=12&t=54297).
  ; Encodes only unallowed characters in an url
  Procedure.s encodeURLWeak_UTF8(input.s)
     Protected result.s = ""
     Protected bufferLength = StringByteLength(input, #PB_UTF8) 
     Protected *buffer.BYTE = AllocateMemory(bufferLength + 1) ;+1 for NULL Character
  
     If *buffer = 0
       Debug "Unable to allocate memory"
        ProcedureReturn ""
     EndIf
     
     ; Convert string to utf8
     PokeS(*buffer, input, -1, #PB_UTF8)
     
     ; Do the encoding
     Protected *pos.BYTE = *buffer
     While *pos\b <> 0
       Protected char.b = *pos\b
       
        If (char >= 'A' And char <= 'Z') Or (char >= 'a' And char <= 'z') Or (char >= '0' And char <= '9')  Or char = '-' Or char = '_' Or char = '.' Or char = '~'
          result + Chr(char)
        
        ; Additionally dont encode the following characters, which might make sense in an URL
        ElseIf char = '!' Or (char >= '#' And char <= ',') Or char = '/' Or char = ':' Or char = ';' Or char = '=' Or char = '?' Or char = '@' Or char = '[' Or char = ']'
        	result + Chr(char)
        	        
        Else
           result + "%" + Right("00"+Hex(Char), 2)
        EndIf
        
        *pos + SizeOf(BYTE)
     Wend
  
     FreeMemory(*buffer)
  
     ProcedureReturn result
  EndProcedure
  
  ; Увы, http.pbi иногда возращает неполные xml-деревья, так што...
  Procedure.s GetURL(url.s)
    ; http://purebasic.mybb.ru/viewtopic.php?id=374
    
    LockMutex(MutexNetwork)
    
    time_for_update.i=10000
    buffer_size.i=4096
    
    #server_ip = "api.vk.com"
    #server_host = "api.vk.com"
    #server_port = 80
    #server_protocol = "HTTP/1.1"
    #delay_part = 25
    
    url.s=encodeURLWeak_UTF8(url.s)
  
    Protected connection_id.i = OpenNetworkConnection(#server_ip, #server_port, #PB_Network_TCP)
    If connection_id <> 0
      Protected string_to_send.s = "GET "+url.s+" " + #server_protocol + #CRLF$
      string_to_send + "Host: " + #server_host + #CRLF$
      string_to_send + "Connection: Close" + #CRLF$ + #CRLF$
      
      SendNetworkString(connection_id , string_to_send)
      
      Protected time_current.i = ElapsedMilliseconds()
      Protected time_limit.i = time_current + time_for_update
      
      Protected final_of_transmission.i = 0
      
      Define content.s
      
      Repeat     
        Select NetworkClientEvent(connection_id)
          Case #PB_NetworkEvent_Data
            Repeat
              Protected *memory_buffer = AllocateMemory(buffer_size)
              Protected received_size.i = ReceiveNetworkData(connection_id, *memory_buffer, buffer_size)
              ;Debug received_size
              If received_size = -1 Or received_size = 0
                FreeMemory(*memory_buffer)
                final_of_transmission.i = 1
                Break
              Else
                Protected received_size_global.i
                received_size_global + received_size
                Protected *memory_global = ReAllocateMemory(*memory_global, received_size_global)
                CopyMemory(*memory_buffer, *memory_global + (received_size_global - received_size), received_size)
                FreeMemory(*memory_buffer)
              EndIf
            ForEver
        EndSelect
        
        If final_of_transmission > 0
          CloseNetworkConnection(connection_id)
          content = PeekS(*memory_global, received_size_global, #PB_UTF8)
          FreeMemory(*memory_global)
          ;Debug "Done!"
          result_html.s = content
          Break
        Else
          ;timeout check call
          time_current = ElapsedMilliseconds()
          If time_current > time_limit
            ;status is timeout (server not responge)
            CloseNetworkConnection(connection_id)
            Debug "GetURL() TimeOut!"
            Break
          Else
            Delay(#delay_part)
          EndIf
        EndIf
      ForEver
    Else
      Debug "GetURL() Can not connect!"
    EndIf 
    
    findxml=FindString(result_html.s, "<?xml")
    If findxml<>0
      result_html.s=Right( result_html.s, Len(result_html.s)-findxml+1 )
    Else
      result_html.s=""
    EndIf
    
    UnlockMutex(MutexNetwork)

    ProcedureReturn result_html.s
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 11
; FirstLine = 8
; Folding = -
; EnableUnicode
; EnableXP