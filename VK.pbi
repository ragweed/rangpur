﻿; Здесь есть готовые процедуры нужных нам функции VK API.
IncludeFile "VKontakteAPI.pbi"

DeclareModule VK
  
  ; Все аудиозаписи.
  #FakeAlbum_All = -1

  Structure Subscribe
    subscribe_id.l
    name.s
  EndStructure
  
  Structure Album
    id.l
    title.s
  EndStructure

  NewList SubscribesList.Subscribe()
  NewList AlbumsList.Album()

  Declare.s GetAudioURL(audio_id, owner_id)
  Declare.s GetNewPosts(subscribe_id, last_date)
  Declare GetSubscribesList(List SubscribesList.Subscribe())
  Declare Init(Token.s="", Secret.s="", UserID.l=0, UserFirstName.s="", UserLastName.s="", UserPhoto.s="")
  Declare AddAudio(audio_id, owner_id, album_id=#PB_Ignore)
  Declare AddLikes(owner_id, item_id)
  Declare GetMusicList(List MusicList.DB::Audio(), album_id=#FakeAlbum_All)
  Declare GetAlbumsList(List AlbumsList.Album())
  
  ; Переменные, через которые данный модуль может сообщить программе, что нужно переавторизироваться.
  Global ReAuth, AuthFinish
  
  ; А здесь будем хранить инфу, небходимую для работы с VK api.
  Global CurrentToken.s, CurrentSecret.s, CurrentUser.l, FirstName.s, LastName.s, UserPhotoURL.s
  
EndDeclareModule

Module VK
  
  ; Осуществляет запрос к VK API.
  ; Умеет информировать об ошибках, с помощью VKAPI::FindError().
  ; В случае успеха возращает ответ в виде xml, иначе xml=""
  Procedure.s GetAPI(url.s)
    Repeat 
      
      try + 1
      xml.s = Network::GetURL(url.s)
      
      error = VKAPI::FindError(xml.s)
      If error<>0
        Select error
          Case VKAPI::#ERROR_MANY_REQUESTS
            Delay(1000)
          Case VKAPI::#ERROR_AUTH_FAILED
            result=MessageRequester("", "Срок действия токена истёк. Необходимо повторно пройти авторизацию.", #PB_MessageRequester_YesNo)
            If result=#PB_MessageRequester_No
              xml.s = ""
              Break
            Else
              ReAuth = 1
              Repeat
                Delay(1000)
              Until AuthFinish = 1
              AuthFinish = 0
            EndIf
          Case VKAPI::#ERROR_UNKNOWN
            result=MessageRequester("", "Произошла неизвестная ошибка VK API."+Chr(10)+VKAPI::CurrentErrorMsg.s+Chr(10)+"Повторить запрос?", #PB_MessageRequester_YesNo)
            If result=#PB_MessageRequester_No
              xml.s = ""
              Break
            EndIf
        EndSelect
      EndIf  
      
      If try>3
        result=MessageRequester("", "Время ожидания сетевого запроса истекло."+Chr(10)+"Пожалуйста, проверьте подключение к интернету и повторите попытку!", #PB_MessageRequester_YesNo)
        If result=#PB_MessageRequester_No
          Break
        Else
          try=0
        EndIf
      EndIf
      
    Until xml.s<>""
    
    ProcedureReturn xml.s
  EndProcedure
  
  ; Эта процедура получит и распарсит информацию о пользователе.
  Procedure GetUserInfo()
    xml.s = GetAPI( VKAPI::users_get( VKAPI::#XML, Str(CurrentUser), "photo_200" ))
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/user/")
      If *CurrentNode
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/user/first_name")
        FirstName.s = GetXMLNodeText(*TempNode)
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/user/last_name/")
        LastName.s = GetXMLNodeText(*TempNode)
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/user/photo_200/")
        UserPhotoURL.s = GetXMLNodeText(*TempNode)
        result = 1
      EndIf
      FreeXML(xml_id)
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Проверяем корректность токена\логинимся в ВК.
  Procedure Init(Token.s="", Secret.s="", UserID.l=0, UserFirstName.s="", UserLastName.s="", UserPhoto.s="")
    If UserID = 0
      ReAuth = 0
      If VKAPI::Login("wall, friends, audio, offline")
        CurrentToken.s = VKAPI::CurrentToken.s
        CurrentSecret.s = VKAPI::CurrentSecret.s
        CurrentUser = VKAPI::CurrentUserID.l
        AuthFinish = 1
        GetUserInfo()
      EndIf
    Else
      VKAPI::Init(Token.s, UserID.l, Secret.s)
      CurrentToken.s = VKAPI::CurrentToken.s
      CurrentSecret.s = VKAPI::CurrentSecret.s
      CurrentUser = VKAPI::CurrentUserID.l
      FirstName.s = UserFirstName.s
      LastName.s = UserLastName.s
      UserPhotoURL.s = UserPhoto.s
    EndIf
  EndProcedure
  
  ; ВНЕЗАПНО, вернёт URL нужного нам аудиофайла.
  Procedure.s GetAudioURL(audio_id, owner_id)
    xml.s = GetAPI( VKAPI::audio_getById( VKAPI::#XML, Str(owner_id)+"_"+Str(audio_id) ) )
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/audio/url/")
      If *CurrentNode
        url.s = GetXMLNodeText(*CurrentNode)
      EndIf
    EndIf
    ProcedureReturn url.s
  EndProcedure
  
  ; ВНЕЗАПНО, вернёт xml с новыми постами за период с last_date по текущее время из группы subscribe_id.
  Procedure.s GetNewPosts(subscribe_id, last_date)
    xml.s = GetAPI( VKAPI::newsfeed_get( VKAPI::#XML, "post", Str(subscribe_id), "", last_date) )
    ProcedureReturn xml.s
  EndProcedure
  
  ; Вернёт список групп/пабликов пользователя.
  ; Если запрос произошёл успешно - верёт колличество подписок, иначе - 0.
  Procedure GetSubscribesList(List SubscribesList.Subscribe())
    ClearList(SubscribesList())
    xml.s = GetAPI( VKAPI::groups_get( VKAPI::#XML, VKAPI::CurrentUserID, 1, "", "", #PB_Ignore, #PB_Ignore ) )
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/group/")
      num_group=1
      While *CurrentNode
        AddElement(SubscribesList())
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/group["+Str(num_group)+"]/id/")
        SubscribesList()\subscribe_id = -Val( GetXMLNodeText(*TempNode) )
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/group["+Str(num_group)+"]/name/")
        SubscribesList()\name = GetXMLNodeText(*TempNode)
        num_group=num_group+1
        *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/group["+Str(num_group)+"]/")
      Wend
      FreeXML(xml_id)
      result = ListSize(SubscribesList())
    Else
      result = 0
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Добавить трек в список аудиозаписей пользователя в ВК. Возвращает идентификатор созданной аудиозаписи. 
  Procedure AddAudio(audio_id, owner_id, album_id=#PB_Ignore)
    xml.s = GetAPI( VKAPI::audio_add( VKAPI::#XML, audio_id, owner_id, album_id ) )
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/")
      If *CurrentNode
        result = Val( GetXMLNodeText(*CurrentNode) )
      EndIf
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Добавит данный пост в список 'Мне нравится' текущего пользователя.
  ; Возвращает колличество пользователей, лайкнувших пост. 
  Procedure AddLikes(subscribe_id, item_id)
    xml.s = GetAPI( VKAPI::likes_add( VKAPI::#XML, "post", subscribe_id, item_id ) )
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/likes/")
      If *CurrentNode
        result = Val( GetXMLNodeText(*CurrentNode) )
      EndIf
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Загружает из VK в MusicList() аудиозаписи пользователя.
  ; Используя album_id можно получить треки конкретного альбома, чтобы получить список
  ; всех записей нужно присвоить значение #FakeAlbum_All.
  ; Вернёт колличество аудиозаписей, иначе 0.
  Procedure GetMusicList(List MusicList.DB::Audio(), album_id=#FakeAlbum_All)
    ClearList(MusicList())
    
    If album_id=#FakeAlbum_All
      album_id = #PB_Ignore
    EndIf
    
    xml.s = GetAPI( VKAPI::audio_get( VKAPI::#XML, CurrentUser, album_id) )
    
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/")
      num_audio = 1
      While *CurrentNode
        AddElement(MusicList())

        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/id/")
        MusicList()\audio_id = Val( GetXMLNodeText(*TempNode) )
        ; Когда информация берётся из БД - fake_id является индификатором трека в БД.
        ; Здесь же нужно просто обеспечить уникальность значения, поэтому fake_id = audio_id,
        ; Понять причину можно порывшись в Action::OpenAudio().
        MusicList()\fake_id = MusicList()\audio_id
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/owner_id/")
        MusicList()\owner_id = Val( GetXMLNodeText(*TempNode) )
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/artist/")
        MusicList()\artist = GetXMLNodeText(*TempNode)
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/title/")
        MusicList()\title = GetXMLNodeText(*TempNode)
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/duration/")
        MusicList()\duration = Val( GetXMLNodeText(*TempNode) )
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/url/")
        MusicList()\url = GetXMLNodeText(*TempNode)

        num_audio + 1
        *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/audio["+Str(num_audio)+"]/")
      Wend
      FreeXML(xml_id)
      result = ListSize(MusicList())
    Else
      result = 0
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Загружает из VK в AlbumsList() альбомы пользователя.
  ; Вернёт колличество альбомов, иначе 0.
  Procedure GetAlbumsList(List AlbumsList.Album())
    ClearList(AlbumsList())
    xml.s = GetAPI( VKAPI::likes_getAlbums( VKAPI::#XML ) )
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/")
      num_album = 1
      While *CurrentNode
        AddElement(AlbumsList())
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/album["+Str(num_album)+"]/id/")
        AlbumsList()\id = Val( GetXMLNodeText(*TempNode) )
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/album["+Str(num_album)+"]/title/")
        AlbumsList()\title = GetXMLNodeText(*TempNode)
        num_album + 1
        *CurrentNode = XMLNodeFromPath(MainXMLNode(xml_id), "/response/items/album["+Str(num_album)+"]/")
      Wend
      FreeXML(xml_id)
      result = ListSize(AlbumsList())
    EndIf
    ProcedureReturn result
  EndProcedure  

EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 34
; FirstLine = 12
; Folding = 8--
; EnableUnicode
; EnableXP