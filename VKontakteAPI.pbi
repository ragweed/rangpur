﻿; Title       : VKontakteAPI.pbi
;
; Author      : ragweed
; Created     : 05.05.15
; Last Update : 06.04.16
; Version     : 0.5.0
; Compiler    : PureBasic 5.31 (x86)
; Platform    : Windows, Linux
; Links       : https://bitbucket.org/ragweed/

DeclareModule VKAPI

  #CLIENT_ID    = 4975260
  #API_VERSION  = "5.50"
  #NOHTTPS      = 1
  
  #XML  = 1
  #JSON = 0
  
  #ERROR_UNKNOWN        = -1
  #ERROR_AUTH_FAILED    = 5
  #ERROR_MANY_REQUESTS  = 6
  
  Global CurrentToken.s, CurrentSecret.s, CurrentUserID.l, CurrentErrorMsg.s
  
  Declare Login(scope.s)
  Declare FindError(xml.s)
  Declare.s audio_getById( response, audios.s )
  Declare Init(Token.s, UserID.l, Secret.s="")
  Declare.s newsfeed_get( response, filters.s="", source_ids.s="", start_from.s="", start_time=#PB_Ignore, 
                          end_time=#PB_Ignore, count=#PB_Ignore, return_banned=#PB_Ignore, max_photos=#PB_Ignore )
  Declare.s audio_get( response, owner_id=#PB_Ignore, album_id=#PB_Ignore, audio_ids.s="", 
                         need_user=#PB_Ignore, offset=#PB_Ignore, count=#PB_Ignore )
  Declare.s groups_get( response, user_id, extended, filter.s, fields.s, offset, count )
  Declare.s users_get( response, user_ids.s="", fields.s="", name_case.s="" )
  Declare.s audio_add( response, audio_id, owner_id, album_id=#PB_Ignore, group_id=#PB_Ignore )
  Declare.s likes_getAlbums( response, owner_id=#PB_Ignore, offset=#PB_Ignore, count=#PB_Ignore )
  Declare.s likes_add( response, type.s, owner_id, item_id, access_key.s="", ref.s="" )

EndDeclareModule

Module VKAPI
  
  Procedure.s Protocol()
    If #NOHTTPS = 1
      result.s = "http://"
    Else
      result.s = "https://"
    EndIf
    ProcedureReturn result.s
  EndProcedure
  
  ;- Авторизация \ Установка текущих Token\Secret\UserID.
  ;{
  
  Procedure Init(Token.s, UserID.l, Secret.s="")
    CurrentToken.s = Token.s
    CurrentUserID.l = UserID.l
    CurrentSecret.s = Secret.s
  EndProcedure
  
  ; Формирует URL диалога авторизации OAuth.
  Procedure.s AuthCreateURL(scope.s)
    ; http://vk.com/dev/auth_mobile
    ; http://vk.com/dev.php?method=api_nohttps
    URL_Authorize.s = Protocol()+"oauth.vk.com/authorize?"
    URL_Authorize.s + "client_id="+Str(#CLIENT_ID)+"&" ; ID программы, получаем здесь: http://vk.com/dev;
    If #NOHTTPS = 1
      nohttps_scope.s = ", nohttps"
    EndIf
    URL_Authorize.s + "scope="+scope.s+nohttps_scope.s+"&" ; Запрашиваемые права доступа приложения; 
    URL_Authorize.s + "redirect_uri="+Protocol()+"oauth.vk.com/blank.html&" ; redirect_uri=https://oauth.vk.com/blank.html , и не спрашивай почему! >_<
    URL_Authorize.s + "display=mobile&"
    URL_Authorize.s + "v="+#API_VERSION+"&" ; Актуальная версия API.
    URL_Authorize.s + "response_type=token"
    
    ProcedureReturn URL_Authorize.s
  EndProcedure
  
  ; Извлекает необходимую информацию из URL, полученного по завершении авторизации.
  ; Вернёт 1 вслучае успеха.
  Procedure AuthParsingData(url.s)
    If FindString(url.s, "#access_token", 0)<>0
      url.s = ReplaceString(url.s, "#", "?") ; Если не сделать эту замену, GetURLPart() не осилит нашу строку...
      Token.s = GetURLPart(url.s, "access_token")
      UserID.l = Val(GetURLPart(url.s, "user_id"))
      
      If #NOHTTPS = 1
        Secret.s = GetURLPart(url.s, "secret")
      EndIf

      Init(Token.s, UserID.l, Secret.s)
      ProcedureReturn 1
    Else
      ProcedureReturn 0
    EndIf
  EndProcedure
  
  ; Отобразит окно авторизации в ВК.
  Procedure Login(scope.s)
    URL_Authorize.s = AuthCreateURL(scope.s)
    
    Window_Login = OpenWindow(#PB_Any, #PB_Ignore, #PB_Ignore, 600, 400, "Авторизация", #PB_Window_SystemMenu | #PB_Window_ScreenCentered)
    String_URL = StringGadget(#PB_Any, 5, 5, 560, 25, "")
    SetGadgetText(String_URL, URL_Authorize.s)
    Button_GoTo = ButtonGadget(#PB_Any, 570, 5, 25, 25, ">")
    WebView_Auth = WebGadget(#PB_Any, 5, 35, 590, 360, URL_Authorize.s)

    ;Ждём, ввода логине\пароля и залогина...
    Repeat
      EventId = WaitWindowEvent()
      
      If EventId = #PB_Event_Gadget And EventGadget() = Button_GoTo
        SetGadgetText(WebView_Auth, GetGadgetText(String_URL))
      EndIf
      
      If AuthParsingData(GetGadgetText(WebView_Auth)) = 1
        result = 1
        Break
      EndIf
    Until EventId = #PB_Event_CloseWindow
    CloseWindow(Window_Login)
    ProcedureReturn result
  EndProcedure
  
  ; Ручная авторизация.
  Procedure ManualLogin(scope.s)
    URL_Authorize.s = AuthCreateURL(scope.s)

    Window_FakeLogin = OpenWindow(#PB_Any, #PB_Ignore, #PB_Ignore, 600, 220, "Авторизация", #PB_Window_SystemMenu | #PB_Window_ScreenCentered)
    String_outURL = StringGadget(#PB_Any, 20, 30, 560, 25, "", #PB_String_ReadOnly)
    SetGadgetText(String_outURL, URL_Authorize.s)
    String_inURL = StringGadget(#PB_Any, 20, 100, 560, 25, "")
    FrameGadget(#PB_Any, 10, 10, 580, 60, "Скопируй эту строку и открой в браузере")
    FrameGadget(#PB_Any, 10, 80, 580, 60, "После авторизации скопируй содержимое адресной строки и вставь здесь")
    FrameGadget(#PB_Any, 10, 150, 580, 60, "Жми кнопку")
    Button_AddUser = ButtonGadget(#PB_Any, 20, 170, 560, 30, "Ну лан...") 

    ;Ждём, ввода логине\пароля и залогина...
    Repeat
      EventId=WaitWindowEvent()
      If EventId=#PB_Event_Gadget And EventGadget()=Button_AddUser
        If AuthParsingData(GetGadgetText(String_inURL)) = 1
          result = 1
          Break
        EndIf
      EndIf
    Until EventId=#PB_Event_CloseWindow
    CloseWindow(Window_FakeLogin)
    ProcedureReturn result
  EndProcedure
  
  ;}
  
  ;- Вспомогательные процедуры для методов.
  ;{
  
  Procedure.s md5(s.s)
    ;http://www.purebasic.fr/english/viewtopic.php?f=13&t=61347
    Protected i,mbuf
    mbuf= AllocateMemory(StringByteLength(s))
    For i = 0 To Len(s)-1
      PokeB(mbuf+i,Asc(Mid(s,i+1,1)))
    Next
    PokeB(mbuf+Len(s),0)
    ProcedureReturn MD5Fingerprint(mbuf,Len(s))
  EndProcedure
  
  Procedure.s CreateURL(metod.s, Map Requests.s(), response) 
    ; https://vk.com/dev/api_requests
    ; http://vk.com/dev.php?method=api_nohttps
    ResetMap(Requests())
    While NextMapElement(Requests())
      If Requests()
        request.s + "&"+MapKey(Requests())+"="+Requests()
      EndIf
    Wend
    If response=#XML
      url.s="/method/"+metod.s+".xml?="+request.s+"&v="+#API_VERSION
    Else
      url.s="/method/"+metod.s+"?="+request.s+"&v="+#API_VERSION
    EndIf
    sig.s=md5(url.s+CurrentSecret.s)
    url.s + "&sig="+sig.s
    url.s="http://api.vk.com"+url.s
    ProcedureReturn url.s
  EndProcedure
  
  ; Ищет <error> в xml.s и возращает <error_code>. Иначе вернёт 0.
  ; https://vk.com/dev/errors
  Procedure FindError(xml.s)
    If xml.s<>""
      xml_id = CatchXML(#PB_Any, @xml, StringByteLength(xml)+1, 0, #PB_UTF8)
      *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/error/error_code/")
      If *TempNode
        error_code = Val( GetXMLNodeText(*TempNode) )
        *TempNode = XMLNodeFromPath(MainXMLNode(xml_id), "/error/error_msg/")
        CurrentErrorMsg.s = GetXMLNodeText(*TempNode)
      EndIf
      
      If error_code<>0
        Select error_code
          Case #ERROR_AUTH_FAILED
            ProcedureReturn #ERROR_AUTH_FAILED
          Case #ERROR_MANY_REQUESTS
            ProcedureReturn #ERROR_MANY_REQUESTS
          Default
            ProcedureReturn #ERROR_UNKNOWN
            MessageRequester("VK API Error", Str(error_code)+" "+CurrentErrorMsg.s)
            Debug xml.s
        EndSelect
      EndIf
    EndIf
    ProcedureReturn 0 
  EndProcedure
  
  ;}
  
  ;- Методы.
  ;{
  
  ;https://vk.com/dev/audio.getById
  Procedure.s audio_getById( response, audios.s )
    NewMap Requests.s()
    Requests("access_token") = CurrentToken.s
    Requests("audios") = audios.s
    url.s = CreateURL("audio.getById", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/newsfeed.get
  Procedure.s newsfeed_get( response, filters.s="", source_ids.s="", start_from.s="", start_time=#PB_Ignore, 
                                       end_time=#PB_Ignore, count=#PB_Ignore, return_banned=#PB_Ignore, max_photos=#PB_Ignore )
    NewMap Requests.s()
    Requests("access_token") = CurrentToken.s
    Requests("filters") = filters.s
    If return_banned<>#PB_Ignore : Requests("return_banned") = Str(return_banned) : EndIf
    If start_time<>#PB_Ignore : Requests("start_time") = Str(start_time) : EndIf
    If end_time<>#PB_Ignore : Requests("end_time") = Str(end_time) : EndIf
    If max_photos<>#PB_Ignore : Requests("max_photos") = Str(max_photos) : EndIf
    If source_ids.s<>"" : Requests("source_ids") = source_ids.s : EndIf
    If start_from.s<>"" : Requests("start_from") = start_from.s : EndIf
    If count<>#PB_Ignore : Requests("count") = Str(count) : EndIf
    
    url.s = CreateURL("newsfeed.get", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/groups.get
  Procedure.s groups_get( response, user_id, extended, filter.s, fields.s, offset, count )
    NewMap Requests.s()
    Requests("access_token") = CurrentToken.s
    Requests("user_id") = Str(user_id)
    If extended<>#PB_Ignore : Requests("extended") = Str(extended) : EndIf
    Requests("filter")=filter.s
    Requests("fields")=fields.s
    If offset<>#PB_Ignore : Requests("offset")=Str(offset) : EndIf
    If count<>#PB_Ignore : Requests("count")=Str(count) : EndIf

    url.s = CreateURL("groups.get", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/users.get
  Procedure.s users_get( response, user_ids.s="", fields.s="", name_case.s="" )
    NewMap Requests.s()
    Requests("access_token")  = CurrentToken.s
    If user_ids.s<>""   : Requests("user_ids")      = user_ids.s  : EndIf
    If fields.s<>""     : Requests("fields")        = fields.s    : EndIf
    If name_case.s<>""  : Requests("name_case")     = name_case.s : EndIf
    
    url.s = CreateURL("users.get", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/audio.get
  Procedure.s audio_get( response, owner_id=#PB_Ignore, album_id=#PB_Ignore, audio_ids.s="", 
                         need_user=#PB_Ignore, offset=#PB_Ignore, count=#PB_Ignore )
    NewMap Requests.s()
    Requests("access_token") = CurrentToken.s
    If owner_id<>#PB_Ignore : Requests("owner_id") = Str(owner_id) : EndIf
    If album_id<>#PB_Ignore : Requests("album_id") = Str(album_id) : EndIf
    If audio_ids.s<>"" : Requests("audio_ids")=audio_ids.s : EndIf
    If need_user<>#PB_Ignore : Requests("need_user") = Str(need_user) : EndIf
    If offset<>#PB_Ignore : Requests("offset") = Str(offset) : EndIf
    If count<>#PB_Ignore : Requests("count") = Str(count) : EndIf
    
    url.s = CreateURL("audio.get", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/audio.add
  Procedure.s audio_add( response, audio_id, owner_id, album_id=#PB_Ignore, group_id=#PB_Ignore )
    NewMap Requests.s()
    Requests("access_token") = CurrentToken.s
    Requests("audio_id") = Str(audio_id)
    Requests("owner_id") = Str(owner_id)
    If album_id<>#PB_Ignore : Requests("album_id")=Str(album_id) : EndIf
    If group_id<>#PB_Ignore : Requests("group_id")=Str(group_id) : EndIf

    url.s = CreateURL("audio.add", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/likes.add
  Procedure.s likes_add( response, type.s, owner_id, item_id, access_key.s="", ref.s="" )
    NewMap Requests.s()
    Requests("access_token") = CurrentToken.s
    Requests("type") = type.s
    Requests("owner_id") = Str(owner_id)
    Requests("item_id") = Str(item_id)
    
    If access_key.s<>"" : Requests("access_key") = access_key.s : EndIf
    If ref.s<>"" : Requests("ref") = ref.s : EndIf

    url.s = CreateURL("likes.add", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;https://vk.com/dev/audio.getAlbums
  Procedure.s likes_getAlbums( response, owner_id=#PB_Ignore, offset=#PB_Ignore, count=#PB_Ignore )
    NewMap Requests.s()
    
    Requests("access_token") = CurrentToken.s
    If owner_id<>#PB_Ignore : Requests("owner_id") = Str(owner_id) : EndIf
    If offset<>#PB_Ignore : Requests("offset") = Str(offset) : EndIf
    If count<>#PB_Ignore : Requests("count") = Str(count) : EndIf
    
    url.s = CreateURL("audio.getAlbums", Requests(), response)
    FreeMap(Requests())
    ProcedureReturn url.s
  EndProcedure
  
  ;}
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 34
; FirstLine = 12
; Folding = b5--
; EnableUnicode
; EnableXP